install:
	sudo luarocks make

VERSION := 0.8.0-1

upload:
	luarocks upload rockspecs/cmd4lua-$(VERSION).rockspec

upload-with-api-key:
	$(eval API_KEY=$(shell bash ${HOME}/.config/dmenu-scripts/dm-api luarocks))
	# [ -z "$(API_KEY)" ] && echo "No defined LUAROCKS_API_KEY" && exit 1
	luarocks upload rockspecs/cmd4lua-$(VERSION).rockspec --api-key=$(API_KEY)


