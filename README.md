Synopsis
--------


`Cmd4Lua` is a [Lua] module that allows you to quickly and easily write complex command handlers for cli-like applications.

Support nested (tree-like) commands with options and arguments.
Allows you to write code in a simple and framework style.
With automatic building of help and its display in case of incorrect input.

The main goal of `Cmd4Lua` module is to provide the ability to write handlers
so that there is no need to manually create detailed help on using commands.

So that detailed help is automatically created, if necessary,
right in runtime according to the annotations written in the code.

Requirements
------------

* [Lua] 5.1+ **or** [LuaJIT] 2.0+


Installation
------------

To install the latest release

```sh
git clone https://gitlab.com/lua_rocks/cmd4lua
cd cmd4lua
luarocks make
```

or
```sh
luarocks install https://gitlab.com/lua_rocks/cmd4lua/-/raw/master/cmd4lua-scm-1.rockspec?ref_type=heads
```

Install via luarocks
```sh
luarocks install cmd4lua
```

Note:

This laurock package has built-in features to generating debug messages.
the package must be installed on the system
в системе должен быть установлен пакет
In order for this feature will be available, the `dprint` luarock package
must be installed on the system. This package is a "optional" dependency
and `cmd4lua` can work without it, but if you need to have access to debug
messages in your application,
you can install [dprint](https://gitlab.com/lua_rocks/dprint) additionally:

```sh
git clone https://gitlab.com/lua_rocks/dprint
cd cmd4lua
luarocks make
```


Warnings
--------

This is a alfa release. Changes can be made in the future

Examples
--------
See the [examples simple](./examples/simple.lua) directory
See the [examples framework](./examples/framework.lua) directory

In both variants, when you enter the help command at any nesting level,
instead of the work itself, detailed help about the entered data will be shown.

If unexpected arguments or options are entered that are not accessed in the
code of the command handler, a warning will also be displayed.
With a detailed display of what was entered incorrectly and what is expected
from the user.

To run the script:
```sh
./examples/framework.lua --help
# or
cd examples/
./framework.lua --help
```

## How to pass string begins with a dash as arg

For cases when you need to pass a string starting with a dash as an argument,
but not as an optional key you can use a `--` meaning that the description of
all options is completed and only arguments go further.

Example:

```sh
lua ./examples/framework.lua echo -- -arg-begins-dash
# the value of optional key is "nil"
# the value of arg1 is "-arg-begins-dash"
```

Debugging
---------
By default, debug output for modules of `cmd4lua` is disabled.
to enable debug otput for cmd4lua itself use:
```
some-command --_debug [true cmd4lua]
```
