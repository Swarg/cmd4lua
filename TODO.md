find . -type f -name '*.lua' -exec sed -i 's/quite/quiet/g' {} \; -ls

arg eats up --help for subcommand and is_input_valid does not show help!
function M.cmd_link(w)
  w:usage(':EnvLang link list -- input". 1" to link current file to 1 in list')

  local fn = w:desc('file in the project to be linked')
      :def(vim.api.nvim_buf_get_name(0)):opt('--file', '-f')
  local act = w:desc():pop():optional():arg() -- < --help

  if not w:is_input_valid() then return end --
