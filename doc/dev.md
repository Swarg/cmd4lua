#

## How can I run all test files of a specific directory?

$ busted -p ".*%.lua$" tests

This tells busted to run all *.lua files inside the tests directory.
By default busted will only run test files that have a _spec in the file name.
So you have to use the -p option to tell busted to match against all files.
This will also run all test files in any sub directory, so if that is not desired
then you will have to use a script to avoid recursing into any sub directories.

```sh
for file in test/*_spec.lua; do busted "$file"; done
```


## how to check installation via lua-repl

```sh
lua -lcmd4lua
> local Cmd4Lua = require('cmd4lua'); print(Cmd4Lua.of('its-works!'):arg(0))
1
```

## Where to put the API key

so as not to specify it every time on the command line

```sh
cat ~/.luarocks/upload_config.lua
```
```
key = "API_KEY_HERE"
server = "https://luarocks.org"
```


## Issue with doc - directory

Any luarocks package must have a doc directory.
`luarocks pack` and `luarocks upload` interact with Git and take files from the
git-repo (index) and not from the current directory where the package is
installed.

When creating a rock(luarock package), no errors may be displayed stating that
the required doc directory is not included in the rock-archive.
But installing such a package from the central repository will not work -
will give an error stating that there is no `doc` directory

Before upload own package you can check  if the directory is in rock:

```sh
unzip -l *.rock | grep doc
```
