#!/usr/bin/env lua
local Cmd4Lua = require("cmd4lua")
local M = {}

--[[
The task that the mechanics of adding errors solves is to make it possible to
add errors inside complex actions in order to stop further execution of the
action.
First of all, it is for such checks that
need to be performed precisely during the execution of the action of the command
and not before the action itself is launched at the user input validation stage.

In this example, we consider two cases without an error and with an error that
occurred during the execution of the action.

To emulate the occurrence of an error, you need to specify the word "error" as
the first argument(Case2)

-- Case-1: no errors:

./examples/exitcode.lua action arg ; echo $?

The work action for this command has started
Somewhere in the middle of doing useful work
This is where the rest of the work for this action is done.
Done.
0


-- Case-2: with errors:
--
/examples/exitcode.lua action error ; echo $?

The work action for this command has started
Somewhere in the middle of doing useful work
[WARN]: An error occurred while performing the action

1

]]

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(args)
  return
      Cmd4Lua.of(args)
      :about("Description of the App")
      :handlers(M)

      :desc('Some action')
      :cmd('action', "a")

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

-- handler of the command "action"
---@param w Cmd4Lua
function M.cmd_action(w)
  -- parse input
  local arg = w:desc('the required arg'):pop():arg()

  -- check is all input defined or show help
  if not w:is_input_valid() then return end

  print('The work action for this command has started')

  print('Somewhere in the middle of doing useful work')

  -- here we emulate a certain check that
  -- can be done precisely during the execution of the action and
  -- not at the input validation stage
  if arg == 'error' then
    w:error('[WARN] An error occurred while performing the action')
  end

  if arg == 'error-code' then
    w:error('[WARN] An error occurred while performing the action')
  end

  -- instead of a try-catch block
  if w:has_error() then return end
  -- stop further execution of this command and exit
  -- without pcall (analog of the try-catch block)
  -- this approach allows you to collect a list of several errors instead of
  -- interrupting the execution of the action at the first error.

  print('This is where the rest of the work for this action is done.')
  print('Done.')
end

--------------------------------------------------------------------------------
--                       Main Entrypoint of cli-app
--------------------------------------------------------------------------------

local exitcode = M.handle(arg)
os.exit(exitcode)

