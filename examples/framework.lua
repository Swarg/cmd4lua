#!/usr/bin/env lua
local Cmd4Lua = require("cmd4lua")
local M = {}
local app_name = arg[0] or 'app'

--[[ to run this script use:
cd examples/
./framework.lua --help
]]

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(args)
  Cmd4Lua.of(args)
      :root(app_name)
      :about("Description of the App")
      :handlers(M)

      :desc('the desc of the do-stuff command')
      :cmd('do-stuff', 'ds')

      :desc('use tags and internal storage for variables')
      :cmd('vars', 'v')

      :desc('Show current status')
      :cmd('status', "st")

      :desc('A nested commands')
      :cmd('tree', "t")

      :desc('Say hellow to list of names (def+optl typematching)')
      :cmd('greetings', "hi")

      :desc('echo args and keys')
      :cmd('echo', "e")

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

-- handler of the command "do-stuff"
---@param w Cmd4Lua
function M.cmd_do_stuff(w)
  -- parse input
  local conf = w:desc('a optkey'):env('APP_CONFIG'):opt('--config', '-c')

  local fn = w:desc('the required arg'):pop():arg()
  local cnt = w:desc('the optional arg'):optional():pop():argn()

  -- work
  if w:is_input_valid() then
    print('Config : ', conf)
    print('fn     : ', fn)
    print('ctn    : ', type(cnt), cnt)
  end
end

--
-- use tags and internal storage for variables
--
-- try:
--   lua examples/framework.lua vars
--   lua examples/framework.lua vars arg1
--   lua examples/framework.lua vars arg1 --config-file x 2
--   lua examples/framework.lua vars arg1 123 --config-file path -k kvalue
--
---@param w Cmd4Lua
function M.cmd_vars(w)
  -- parse input
  w
      :desc('name by optkey'):env('APP_CONFIG'):v_opt('--config-file', '-c')
      :desc('second opt-key'):tag('key'):v_opt('--opt-key', '-k')

      :desc('the required arg1'):tag('first'):pop():v_arg()
      :desc('the required arg2'):tag('second'):optional():pop():v_argn()

  -- work
  if w:is_input_valid() then
    print('config_file: ', w.vars.config_file)
    print('key        : ', w.vars.key)
    print('first-arg  : ', w.vars.first)
    print('second-arg : ', w.vars.second)
  end
end

--
-- try:
--   lua examples/framework.lua status --help
--
---@param w Cmd4Lua
function M.cmd_status(w)
  if w:is_input_valid() then
    print('status')
  end
end

-- nested commands
---@param w Cmd4Lua
function M.cmd_tree(w)
  w:about('A tree command with sub commands')
      :cmd('node-a', 'a', M.cmd_tree_node_a)
      :cmd('node-b', 'b', function() print('node-b') end)
      :cmd('node-c', 'c', function() print('node-c') end)
      :run()
end

-- cmd path: "tree node-a sub-node-1 --key 8"
---@param w Cmd4Lua
function M.cmd_tree_node_a(w)
  -- if not w:is_input_valid() then return end

  w:cmd('sub-node-1', 's1', function(w0)
    local key = w0:opt('--key', '-k')
    if w0:is_input_valid() then
      print('sub-node-1 key:' .. tostring(key))
    end
  end)

  w:cmd('sub-node-2', 's2', function()
    print('sub-node-2')
  end)

  w:run()
  --
end

-- 'def+optl typematching'
---@param w Cmd4Lua
function M.cmd_greetings(w)
  local persons = w:desc('List of names to say hello to')
      :def({ 'Alice' }):optl('--persons', '-p')

  if w:is_input_valid() then
    for _, name in ipairs(persons) do
      print('Hi ' .. tostring(name))
    end
  end
end

--
-- echo args and keys
--
--  lua examples/framework.lua echo -- --key
--
--    the value of optional key is "nil"
--    the value of arg1 is "--key"
--
--  lua examples/framework.lua echo -- -arg-begins-dash
--    the value of optional key is "nil"
--    the value of arg1 is "-arg-begins-dash"
--
--  lua examples/framework.lua echo -arg-begins-dash
--    Unknown Input: Opts:  -arg-begins-dash
--
---@param w Cmd4Lua
function M.cmd_echo(w)
  w:usage('try it:')
  w:usage('./framework.lua echo --key value argumnet')
  w:usage('./framework.lua echo argumnet')
  w:usage('./framework.lua echo -argument-begins-with-dash')
  w:usage('./framework.lua echo -- -argument-begins-with-dash')
  w:usage('./framework.lua echo -- --key')

  local key = w:desc('the optional key with value'):opt('--key', '-k')
  local arg1 = w:desc('the optional argument'):optional():pop():arg()

  if not w:is_input_valid() then return end

  w:say('the value of optional key is "' .. tostring(key) .. '"')
  w:say('the value of arg1 is "' .. tostring(arg1) .. '"')
end

--
if os.getenv('DEBUG_PRINT') == '1' then
  require('cmd4lua.settings').D.enable()
end

M.handle(arg)
