#!/usr/bin/env lua
local Cmd4Lua = require("cmd4lua")

--------------------------------------------------------------------------------
--                        Command Handlers
--------------------------------------------------------------------------------


---@param w Cmd4Lua
local function cmd_say_hellow(w)
  local name = w:desc('the user name'):tag('username'):pop():arg(0)
  --
  if w:is_input_valid() then
    print('hellow ' .. tostring(name))
  end
end

---@param w Cmd4Lua
local function cmd_echo(w)
  local key = w:def('default-value'):opt('--key', '-k')
  local any = w:type('any'):opt('--any', '-a')
  local tbl = w:type('table'):opt('--table', '-t')
  local ls  = w:type('list'):opt('--list', '-l')
  local num = w:optn('--num', '-n')

  local arg = w:optional():type('any'):many():pop():arg(0)

  if w:is_input_valid() then
    print('key: ' .. tostring(key))
    print('any: ' .. tostring(any))
    print('tbl: ' .. tostring(tbl))
    print('ls:  ' .. tostring(ls))
    print('num: ' .. tostring(num))
    print('arg: ' .. tostring(arg))

    while w:has_args() do
      print(tostring(w:type('any'):pop():arg(0)))
    end
  end
end

local M = {}

-- nested commands
---@param w Cmd4Lua
local function cmd_tree(w)
  --
  if w:is_cmd('node-a', 'a') then
    M.cmd_tree_node_a(w)
  elseif w:is_cmd('node-b', 'b') then
    print('node-b')
  elseif w:is_cmd('node-c', 'c') then
    print('node-c')
  end

  -- here if there is a help-request or incorrect input, a help will be showed
  w:show_help_or_errors()
  -- Note:
  -- This is a node where subcommands are dispatched and their handlers are
  -- searched, so you should place a condition-guard at the end of the function
  -- The idea here is that when an argument is checked by is_cmd(), it is
  -- registered as a known command and then a list of all known commands can
  -- be shown
end

-- cmd path: "tree node-a sub-node-1 --key 8"
--        or "t na s1 --key value"
---@param w Cmd4Lua
function M.cmd_tree_node_a(w)
  if w:is_cmd('sub-node-1', 's1') then
    -- parse input
    local key = w:opt('--key', '-k')
    -- before aclual work use the "is_input_valid" (cond-guard)
    -- to be able to stop and show the help upon help-request or invalid input
    if w:is_input_valid() then
      print('sub-node-1 key: ' .. tostring(key))
    end
    --
  elseif w:is_cmd('sub-node-2', 's2') then
    print('sub-node-2')
  end
  --
  -- w:show_help_or_errors() -- You don’t have to specify a this function here
  -- since it will be called when returning to the context of the parent command
end

--------------------------------------------------------------------------------
--                           Entry Point
--------------------------------------------------------------------------------
if os.getenv('DEBUG_PRINT') == '1' then
  require('cmd4lua.settings').D.enable()
end

-- handle cli input
local w = Cmd4Lua.of(arg) -- _G.arg is a args from cli

if w:is_cmd('say-hellow', 'sh') then
  cmd_say_hellow(w)
  --
elseif w:is_cmd('echo', 'e') then
  cmd_echo(w)
  --
elseif w:desc('A nested command')
    :is_cmd('tree', 't') then
  cmd_tree(w)
end

w:show_help_or_errors()

--------------------------------------------------------------------------------

--[[
examples of commands that you can try from the root of the project:

lua examples/simple.lua say-hellow name
lua examples/simple.lua say-hellow               -- expected argument is missing

lua examples/simple.lua echo
lua examples/simple.lua echo arg
lua examples/simple.lua echo --key               -- OptKeys with Invalid values
lua examples/simple.lua echo --key x
lua examples/simple.lua echo --key x 1
lua examples/simple.lua echo --key x a b c
lua examples/simple.lua echo --key x a 'b c'
lua examples/simple.lua echo --key x a [b c] x
lua examples/simple.lua echo --key [b c]         -- warn expected string has tbl
lua examples/simple.lua echo --any [b c]
lua examples/simple.lua echo --table x           -- warn expected table
lua examples/simple.lua echo --list x            -- warn expected table
lua examples/simple.lua echo --list [a b c]
lua examples/simple.lua echo --num x             -- warn expected number


lua examples/simple.lua tree node-a sub-node-1


advanced built-in commands:
lua examples/simple.lua --_debug --_status -_R --_debug_pint hi
...
]]
