--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("cmd4lua.base")
local Cmd4Lua = require("cmd4lua")

describe(".spec.cmd4lua.base", function()
  it("fullname_first", function()
    assert.is_nil(M.fullname_first(nil, nil))
    assert.same('name', M.fullname_first('name', nil))
    assert.same('full-name', M.fullname_first('name', 'full-name'))
    assert.same(false, M.fullname_first(false, 'name'))
    assert.same('name', M.fullname_first('ns', 'name'))
    assert.same('ns', M.fullname_first('ns'))
  end)

  it("get_max_str_leng", function()
    local lines = { { sn = 'abc' }, { sn = 'a' }, { sn = '1234' }, { sn = '12' } }
    assert.same(4, M.get_max_str_leng(lines, 'sn'))
  end)

  it("get_max_str_leng 2", function()
    local lines = {
      { n = 'abc',  sn = 'a' },
      { n = '1',    sn = '123' },
      { n = 'defj', sn = '12' },
      { n = '12',   sn = '1' },
    }
    local max1, max2 = M.get_max_str_leng(lines, 'n', 'sn')
    assert.same(4, max1)
    assert.same(3, max2)
  end)

  it("get_max_str_leng 3", function()
    local lines = {
      { n = 'abc',  sn = 'a',   d = nil },
      { n = '1',    sn = '123', d = '12345678' },
      { n = 'defj', sn = '12',  d = '123' },
      { n = '12',   sn = '1',   d = '12345' },
    }
    local max1, max2, max3 = M.get_max_str_leng(lines, 'n', 'sn', 'd')
    assert.same(4, max1)
    assert.same(3, max2)
    assert.same(8, max3)
  end)

  it("is_optkey", function()
    assert.same(true, M.is_optkey('-k'))
    assert.same(true, M.is_optkey('--key'))
    assert.same(true, M.is_optkey('--key0'))
    assert.same(true, M.is_optkey('--'))
    assert.same(false, M.is_optkey('--0key'))
    assert.same(false, M.is_optkey('--0'))
  end)

  it("is_instanceof_cmd4lua", function()
    assert.same(true, M.is_instanceof_cmd4lua(Cmd4Lua:new()))
    assert.same(true, M.is_instanceof_cmd4lua(Cmd4Lua:new({ arg = 'arg' })))
    assert.same(true, M.is_instanceof_cmd4lua(Cmd4Lua.of('arg -key 1')))
  end)

  it("get_cmdhandler_methodname", function()
    assert.same('N', string.upper('n'))
    M.debug = true
    assert.same('cmdNewClass', M.get_cmdhandler_methodname('new-class'))
    assert.same('cmdClass', M.get_cmdhandler_methodname('class'))
    assert.same('cmdTheWordOfName', M.get_cmdhandler_methodname('the-word-of-name'))
    assert.same('cmdTheWordOf', M.get_cmdhandler_methodname('the-word-of-'))
    assert.same('cmdKeep_underscore', M.get_cmdhandler_methodname('keep_underscore'))
  end)

  it("shallow_copy table", function()
    local orig = { 1, 2, 3 }
    local copy = M.shallow_copy(orig)
    assert.same(orig, copy)
    copy[#copy + 1] = 4
    assert.same(4, copy[4])
    assert.is_nil(orig[4])
    assert.is_not.same(orig, copy)
  end)

  it("shallow_copy not a table", function()
    assert.is_nil(M.shallow_copy(nil))
    assert.same('', M.shallow_copy(''))
    assert.same('a', M.shallow_copy('a'))
    assert.same(true, M.shallow_copy(true))
    assert.same(123, M.shallow_copy(123))
  end)

  it("set_value_to", function()
    local t = {}
    local f = M.set_value_to

    assert.same({ root = { key = 16 } }, f(t, 'root.key', 16))
    assert.same({ root = { key2 = 8, key = 16 } }, f(t, 'root.key2', 8))
    assert.same({ root = { key = 16 } }, f(t, 'root.key2', nil))
    assert.same({ root = false }, f(t, 'root', false)) -- rewrite old value
    assert.same({}, f(t, 'root', nil))                 -- remove "variable"

    local exp = { a = { b = { c = { d = { e = 1 } } } } }
    assert.same(exp, f(t, 'a.b.c.d.e', 1)) -- rewrite old value
  end)

  it("set_value_to without autocreating a subtables", function()
    local t = {}
    local f = M.set_value_to

    -- false -- do not create subtables in the path
    assert.same({ root = 16 }, f(t, 'root', 16, false))
    assert.same(false, f(t, 'a.b', 16, false))
    t['a'] = {}
    assert.same({ a = { b = 16 }, root = 16 }, f(t, 'a.b', 16, false))
  end)
end)
