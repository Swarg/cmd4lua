--
require 'busted.runner' ()
local assert = require('luassert')

local conf = require('cmd4lua.settings')
local parser -- used cmd4lua.builtin
local D = conf.D

describe('cmd4lua.builtin', function()
  setup(function()
    -- expose private function for testing
    _G._TEST = true
    Cmd4Lua = require("cmd4lua")
    M = require("cmd4lua.builtin")
    -- print('remove_opt', M._remove_opt)
    BH = require("cmd4lua.help.builder")
    parser = require("cmd4lua.parser")
    -- Disable display hints about built-in help
    conf.show_help_hints = false
  end)

  teardown(function()
    _G._TEST = nil
  end)

  after_each(function()
  end)

  before_each(function()
    D.on = false
    conf.show_help_hints = false
  end)

  it("remove_opt", function()
    local w = Cmd4Lua.of({ '--key', 'val', '-k', 'v2' })
    assert.same('val', w:opt('--key'))
    assert.same('v2', w:opt('-k'))
    assert.same(true, w:has_opts())
    assert.is_not_nil(_G._TEST)
    assert.same("v2", M._remove_opt(w.keys, '--key', '-k'))
    assert.same(nil, M._remove_opt(w.keys, '--key', '-k'))
    assert.same(false, w:has_opts())
  end)

  it("built-in_commands enable debug mode for dprint global", function()
    if D._VERSION == 'stub' then return end
    D.set_silent_print()

    local w = Cmd4Lua.of('--_debug')
    assert.same(true, D.is_enabled())
    assert.same(false, D.is_enabled(conf))
    assert.same(false, w:has_opts())
    --
    assert.same(true, D.is_enabled())
    w = Cmd4Lua.of('--_debug false')
    assert.same(false, D.is_enabled())
    assert.same(false, D.is_enabled(conf))
  end)

  --
  --
  it("built-in_commands enable dprint for cmd4lua", function()
    if D._VERSION == 'stub' then return end
    D.set_silent_print()
    local res = ''
    local overrides = {
      print_callback = function(line)
        res = res .. tostring(line)
      end
    }

    local w = Cmd4Lua.of('--_debug [true cmd4lua]', overrides)
    assert.same(true, D.is_enabled())
    assert.same(true, D.is_enabled(conf))
    assert.same(false, w:has_opts())
    assert.same('dprint for: cmd4lua.settings is true', res)
    --
    assert.same(true, D.is_enabled())
    Cmd4Lua.of('--_debug false')
    assert.same(false, D.is_enabled())
    assert.same(false, D.is_enabled(conf))

    Cmd4Lua.of('--_debug true')
    assert.same(true, D.is_enabled())
    assert.same(true, D.is_enabled(conf))
  end)

  --
  --
  it("apply_implementations print_callback", function()
    local w = Cmd4Lua:new()
    -- defaults
    assert.same(_G.print, w.interface.print_callback)
    assert.same(nil, w.interface.print_userdata)
    assert.same(parser.parse_line, w.interface.parse_line)
    --
    local res1, res2 = '', ''
    local overrides = {
      print_callback = function(line)
        res1 = res1 .. tostring(line)
      end
    }
    assert.same(1, M.apply_implementations(w, overrides))
    assert.same(overrides.print_callback, w.interface.print_callback)
    w:print('check-1')
    assert.same('check-1', res1)
    -- another way to override print
    local pcb2 = function(line) res2 = res2 .. line end
    w:set_print_callback(pcb2, nil)
    w:print('check-2')
    assert.same('check-1', res1)
    assert.same('check-2', res2)
  end)

  it("apply_implementations generate_handle", function()
    local w = Cmd4Lua:new()
    -- defaults
    assert.same(_G.print, w.interface.print_callback)
    assert.same(nil, w.interface.print_userdata)
    assert.same(parser.parse_line, w.interface.parse_line)

    local output
    local prev_print = _G.print
    ---@diagnostic disable-next-line: duplicate-set-field
    _G.print = function() end -- stub to skip print to stdout
    ---@diagnostic disable-next-line: duplicate-set-field
    _G.print = function(s) output = s end
    ---@diagnostic disable-next-line: inject-field
    w.dev_generate_cmd = { 'a', 'b' }
    M.dev_generate(w)

    assert.match('This is an interface function ', output)
    --
    local args, state
    local overrides = {
      generate_handle = function(args0, state0)
        args, state = args0, state0
        print('from overrided generate_handle')
      end
    }
    assert.same(1, M.apply_implementations(w, overrides))
    assert.same(overrides.generate_handle, w.interface.generate_handle)

    M.dev_generate(w)
    assert.same('from overrided generate_handle', output)
    assert.same(w.dev_generate_cmd, args)
    assert.same(w, state)

    finally(function()
      D.on = false
      D.enable_module(conf, false)
      _G.print = prev_print
    end)
  end)

  it("status standard", function()
    local w = Cmd4Lua:new()
    local e1, e2, e3, e4
    e1 = "generate_handle     standard     function: 0x[%w]+"
    e2 = "parse_line          standard     function: 0x[%w]+"
    e3 = "print_callback      standard     function: 0x[%w]+ %(_G.print%)"
    e4 = "fix_input_callback  standard     false"
    assert.is_table(w.interface)
    local res = M.status(w)
    assert.match(e1, res)
    assert.match(e2, res)
    assert.match(e3, res)
    assert.match(e4, res)
  end)

  it("status", function()
    local I = {
      generate_handle = function() end,
      parse_line = function(line) return parser.parse_line(line) end,
      print_callback = function() end,
      print_callback_userdata = function() end,
      fix_input_callback = function() end,
    }

    local w = Cmd4Lua.of('x', I):self_print_callback_userdata()
    local e1, e2, e3, e4, e5
    e1 = "generate_handle          overridden   " .. tostring(I.generate_handle)
    e2 = "parse_line               overridden   " .. tostring(I.parse_line)
    e3 = "print_callback           overridden   " .. tostring(I.print_callback)
    e4 = "print_callback_userdata  overridden   self"
    e5 = "fix_input_callback       overridden   " .. tostring(I.fix_input_callback)

    local res = M.status(w)
    assert.match(e1, res)
    assert.match(e2, res)
    assert.match(e3, res)
    assert.match(e4, res)
    assert.match(e5, res)
  end)

  -- visual
  it("status", function()
    local I = {
      generate_handle = function() end,
      print_callback_userdata = function() end,
    }

    local w = Cmd4Lua.of('x', I):self_print_callback_userdata()
    local e1, e2, e3, e4, e5
    e1 = "generate_handle          overridden   " .. tostring(I.generate_handle)
    e2 = "parse_line               standard     " .. tostring(w.interface.parse_line)
    e3 = "print_callback           standard     " .. tostring(w.interface.print_callback)
    e4 = "print_callback_userdata  overridden   self"
    e5 = "fix_input_callback       standard     false"
    -- not shown fix_input_callback
    local res = M.status(w)
    assert.match(e1, res)
    assert.match(e2, res)
    assert.match(e3, res)
    assert.match(e4, res)
    assert.match(e5, res)
  end)
end)
