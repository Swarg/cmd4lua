-- 25-10-2023 @author Swarg
--
-- for testing Cmd4Lua class
--
require 'busted.runner' ()

local base = require('cmd4lua.base')
local conf = require('cmd4lua.settings')
local parser = require("cmd4lua.parser")
local D = conf.D

describe('cmd4lua', function()
  local res_lines = nil
  setup(function()
    -- expose private function for testing
    _G._TEST = true
    Cmd4Lua = require 'cmd4lua'
    HB = require("cmd4lua.help.builder")
    HC = require("cmd4lua.help.collector")
    -- Disable display hints about built-in help
    conf.show_help_hints = false
    conf.devmode = true
  end)

  teardown(function()
    _G._TEST = nil
  end)

  before_each(function()
    D.on = false
    conf.show_help_hints = false
    res_lines = nil
  end)

  after_each(function()
  end)


  local print0 = function(line)
    res_lines = res_lines or {}
    res_lines[#res_lines + 1] = line
  end

  ---@param args string|table
  ---@return Cmd4Lua
  ---@param implementions table|nil
  local function new_w(args, implementions)
    -- need to override default parse_line function before call new object
    local w = Cmd4Lua.of(args, implementions):set_print_callback(print0)
    ---@cast w Cmd4Lua
    return w
  end

  -- from w:build_help())
  local function get_help()
    return (res_lines or {})[1]
  end

  -- create stub callback for cmd
  -- UseCase:  to be sure that the corresponding command was run
  ---@param out table
  ---@param cmd_name string
  ---@return function
  local function mk_cmd_callback(out, cmd_name, module)
    assert(type(cmd_name) == 'string', 'cmd_name')

    local handler = function(w, userdata)
      local _name = cmd_name
      local _out = out
      D.dprint('handler callback for cmd_name:', _name)
      if type(_out) == 'table' then
        _out.name = _name
        _out.userdata = userdata
        _out.w = w
      end
    end
    if type(module) == 'table' then
      local func_name = base.get_cmdhandler_funcname(cmd_name)
      assert(type(func_name) == 'string' and func_name ~= '', 'func_name')
      module[func_name] = handler
    end
    return handler
  end

  --------------------------------------------------------------------------------
  it("tool print0", function()
    local w = new_w({ 'root', 'subcmd' })
    w:arg(0)
    assert.same(false, w:is_input_valid())
    local exp = { "Has unexpected Arguments: 'subcmd'\n\nUsage:\n <Arg1>\n" }
    assert.same(exp, res_lines)
  end)

  it("_find_command_handler fom handlers table", function()
    local module = {
      ---@diagnostic disable-next-line: unused-local
      cmd_do_stuff = function(w0, userdata) end,
      ---@diagnostic disable-next-line: unused-local
      cmd_another_one = function(w0, userdata) end,
    }
    local w = new_w({ 'root', 'subcmd' })
        :handlers(module)
    local c = w.collector

    assert.same(module, w.collector.thelp.handlers)
    assert.same("function", type(c:find_cmdhandler_func({ name = 'do-stuff' })))
  end)

  it("_find_command_handler via mk_cmd_callback", function()
    local module, res = {}, {}
    local cb1 = mk_cmd_callback(res, 'do-stuff', module)
    local cb2 = mk_cmd_callback(res, 'another-one', module)
    -- sure new functions created and added to module-table
    assert.same(cb1, module.cmd_do_stuff)
    assert.same(cb2, module.cmd_another_one)

    local w = new_w({ 'do-stuff', 'arg' })   -- input
        :handlers(module)                    -- register functions for commands
        :cmd('do-stuff'):userdata({ '888' }) -- register command but not call

    local c = w.collector
    assert.same(module, w.collector.thelp.handlers)
    assert.same(cb1, c:find_cmdhandler_func({ name = 'do-stuff' }))
    assert.same(cb2, c:find_cmdhandler_func({ name = 'another-one' }))

    assert.same({}, res) -- cmd_do_stuff not called yet
    w:run()              -- find command for current arg and run handler(function)
    -- make sure that the function "cmd_do_stuff" was actually called
    assert.same('do-stuff', res.name)
    assert.same({ '888' }, res.userdata)
    assert.same(w, res.w)
  end)

  it("cmd via callbacks", function()
    local w = new_w({ 'cmd-b' })
    local res
    w:cmd('cmd-a', 'a', function() res = 'a' end)
    w:cmd('cmd-b', 'b', function() res = 'b' end)
    w:cmd('cmd-c', 'c', function() res = 'c' end)
    w:cmd('cmd-d', 'd', function() res = 'd' end)
    w:run()

    assert.same('b', res)
    assert.same(false, w:has_unresolved_cmd())
    assert.same(false, w:has_input_errors())
  end)

  it("cmd via callbacks 2", function()
    local w = new_w({ 'cmd-b' })
    local res = {}
    w:cmd('cmd-a', mk_cmd_callback(res, 'cmd-a'))
    w:cmd('cmd-b', mk_cmd_callback(res, 'cmd-b'))
    w:cmd('cmd-c', mk_cmd_callback(res, 'cmd-c'))
    w:cmd('cmd-d', mk_cmd_callback(res, 'cmd-d'))
    w:run()

    assert.same('cmd-b', res.name)
    assert.same(false, w:has_unresolved_cmd())
    assert.same(false, w:has_input_errors())
  end)

  it("cmd via handlers + run success", function()
    local w = new_w({})
    local module = {
      ---@diagnostic disable-next-line: unused-local
      cmd_do_stuff = function(w0, userdata) end,
      ---@diagnostic disable-next-line: unused-local
      a_one = function(w0, userdata) end,
    }
    w:handlers(module)
        :cmd('do_stuff')
        :tag('a_one'):cmd('another-one')
        :run()

    assert.same(module, w.collector.thelp.handlers)
    local c = w.collector
    assert.same("function", type(c:find_cmdhandler_func({ name = 'do-stuff' })))
  end)


  it("cmd via handlers + run -- fail", function()
    local w = new_w({ 'acmd' })
    local module = {
      cmd_do_stuff = function() end,
      acmd = function() end,
    }

    ---@diagnostic disable-next-line: inject-field
    conf.devmode = false

    w:handlers(module)
        :cmd('do_stuff')
        :cmd('acmd') -- register is ok, error thrown only on try to call a cmd

    assert.has_error(function()
      w:run() -- throw
    end)

    assert.same(module, w.collector.thelp.handlers)
  end)


  it("cmd via handlers + run -- fail", function()
    local w = new_w({ 'acmd' })
    local module = {
      cmd_do_stuff = function() end,
      acmd = function() end,
    }

    -- in devmode it throw an errors exactly when trying to register a command
    ---@diagnostic disable-next-line: inject-field
    conf.devmode = true
    w:handlers(module)
    w:cmd('do_stuff')
    assert.has_error(function()
      w:cmd('acmd') -- thrown erorr not found cmd_acmd
    end)

    assert.has_error(function()
      w:run() -- throw
    end)

    assert.same(module, w.collector.thelp.handlers)
  end)


  it("cmd", function()
    local w = new_w({ "cmd-b" })
    local res
    local callback_a = function(_, ud) res = 'cmd A passed ' .. tostring(ud) end
    local callback_b = function(_, ud) res = 'cmd B passed ' .. tostring(ud) end

    w:cmd('cmd-a', 'a', callback_a)
    w:cmd('cmd-b', 'b', callback_b, 'userdata')
    w:cmd('cmd-c', 'c', callback_b)
    w:run()

    assert.same('cmd B passed userdata', res)
    -- already cleaned in move to deep subcmd
    assert.is_false(w.collector.cmd_expected)
    assert.same(false, w:has_input_errors())
    local exp = "Usage:\n cmd-b\n"   -- path
    assert.same(exp, w:build_help()) -- help for cmd-b level not for root
  end)


  it("cmd", function()
    local w = new_w({})
    local res
    local callback_a = function() res = 'cmd A' end
    local callback_b = function() res = 'cmd B' end
    local callback_c = function() res = 'cmd C' end

    w:desc('d1'):cmd('cmd-a', 'a', callback_a)
    w:desc('d2'):cmd('cmd-b', 'b', callback_b)
    w:desc('d3'):cmd('cmd-c', 'c', callback_c)
    w:run() -- show help

    assert.is_nil(res)

    assert.same(true, w.collector.cmd_expected)
    assert.same(true, w.collector:has_unresolved_cmd())
    assert.same(true, w:has_input_errors())

    local exp = [[
The command is expected

Usage:
 <Command>
Commands:
   a cmd-a    -  d1
   b cmd-b    -  d2
   c cmd-c    -  d3
]]
    assert.same(exp, get_help()) -- from w:build_help())
  end)


  it("default_cmd", function()
    local w = new_w({ '-k', 'kval' })
    local res
    w:default_cmd('cmd-b')
    w:cmd('cmd-a', function() res = 'a' end)
    w:cmd('cmd-b', function() res = 'b' end)
    w:cmd('cmd-c', function() res = 'c' end)
    w:run()

    assert.same('b', res)
    assert.same('kval', w:opt('--key', '-k'))
    assert.same(false, w:has_input_errors())
  end)

  -- default + is_cmd
  it("default_cmd + cmd with help", function()
    local w = new_w({ 'help' })
    w:default_cmd('cmd-b')
    local i, res = 0, false
    local stub = function() i = i + 1 end
    local func = function() res = true end
    assert.same(true, w:is_help()) -- w.only_help = true
    w:cmd('cmd-a', 'a', stub)
    w:cmd('cmd-b', 'b', func)
    w:cmd('cmd-c', 'c', stub)
    w:run()
    assert.same(false, w:has_input_errors())
    assert.same(false, res) -- command handler not called(only_help mode)
    local exp = [[
Usage:
 <Command>
Commands:
   a cmd-a
*  b cmd-b    -  [Default]
   c cmd-c
]]
    assert.same(exp, get_help())
  end)


  it("default_cmd chain", function()
    local w, res = new_w({}), nil
    w:default_cmd('cmd-c')
        :cmd('cmd-a', 'a', function() res = 'a' end)
        :cmd('cmd-b', 'b', function() res = 'b' end)
        :cmd('cmd-c', 'c', function() res = 'c' end)
        :cmd('cmd-d', 'd', function() res = 'd' end)
        :run()

    assert.same('c', res)
    assert.same(false, w:has_unresolved_cmd())
    assert.same(false, w:has_input_errors())
  end)

  --?? example of wrong usage
  it("cmd first arg second", function()
    local w = new_w({})
    local callback = function(_, _) end
    w:desc('desc A'):cmd('cmd-a', 'a', callback)
    w:desc('desc B'):cmd('cmd-b', 'b', callback)
    w:tag('Argument1'):arg() -- ?
    w:desc('desc C'):cmd('cmd-c', 'c', callback)
    w:run()

    local exp = [[
The command is expected
The expected argument 1 "Argument1" is missing.

Usage:
 <Command> <Argument1> <Command>
Commands:
   a cmd-a    -  desc A
   b cmd-b    -  desc B
   c cmd-c    -  desc C
Arguments:
 1  Argument1
]]
    assert.same(exp, get_help())
  end)


  it("first - pop_arg,  cmd - second", function()
    local w = new_w({})
    local callback = function(_, _) end
    w:desc('varname', 'arg before a command')
        :pop_arg()

    w:about('Command Lvl Description')

    w:desc('desc A'):cmd('cmd-a', 'a', callback)
    w:desc('desc B'):cmd('cmd-b', 'b', callback)
    w:run()

    local exp = [[
The command is expected
The expected argument 1 "varname" is missing.

Command Lvl Description
Usage:
 <varname> <Command>
Commands:
   a cmd-a    -  desc A
   b cmd-b    -  desc B
Arguments:
 1  varname             -  arg before a command
]]
    assert.same(exp, get_help())
  end)


  it("arg first cmd second", function()
    local w = new_w({})
    local callback = function(_, _) end
    w:tag('Argument1'):arg()
    w:desc('desc A'):cmd('cmd-a', 'a', callback)
    w:desc('desc B'):cmd('cmd-b', 'b', callback)
    w:run()

    local exp = [[
The command is expected
The expected argument 1 "Argument1" is missing.

Usage:
 <Argument1> <Command>
Commands:
   a cmd-a    -  desc A
   b cmd-b    -  desc B
Arguments:
 1  Argument1
]]
    assert.same(exp, get_help())
  end)

  it("v_has_opt true", function()
    local w = new_w('--no-date')
    -- D.enable_module(conf, true) -- enable dprint
    w:v_has_opt('--no-date')
    assert.same({ no_date = true }, w.vars)
  end)

  it("v_has_opt variants", function()
    assert.same({ k = true }, new_w('-k'):v_has_opt('-k').vars)
    assert.same({ k = true }, new_w('-k 1'):v_has_opt('-k').vars)
    assert.same({ k = false }, new_w('-k 0'):v_has_opt('-k').vars)
    assert.same({ k = false }, new_w('-k false'):v_has_opt('-k').vars)
    assert.same({ k = true }, new_w('-k abc'):v_has_opt('-k').vars) -- eat "abc"
    -- D.enable_module(conf, true) -- enable dprint
    assert.same({ no_date = false }, new_w(''):v_has_opt('--no-date').vars)
    assert.same({ f = true }, new_w('-f'):v_has_opt('-f').vars)
    assert.same({ f = false }, new_w('-f false'):v_has_opt('-f').vars)
  end)

  --
  -- create a variable "open"
  -- if has no opt-key --not-open then open == true
  -- if has opt-key --not-open then open == false
  it("v_optb default value then no opts", function()
    local function f(input)
      local w = new_w(input)

      w:tag('open'):def(true):defOptVal(false):v_optb('--not-open', '-no')
      --                ^               ^ value on given key without value
      --                value on not existed key

      return w:var('open')
    end

    assert.same(true, f('arg'))             -- no key
    assert.same(false, f('--not-open'))     -- key without value fires defOptVal

    assert.same(true, f('--not-open true')) -- key with value
    assert.same(false, f('--not-open 0'))
  end)

  --
  --
  it("v_opt add value of opt to vars - success", function()
    local res_opt, res_arg = nil, nil
    local callback = function(_, _) end
    local callback_b = function(w)
      res_opt = w.vars.opt1 -- same w:get_var('opt1')
      res_arg = w:arg(0)
    end

    ---@diagnostic disable-next-line: unused-local
    local w = new_w({ 'cmd-b', '--key', 'kvalue', 'arg' })
        :desc('desc A'):cmd('cmd-a', 'a', callback)
        :desc('desc B'):cmd('cmd-b', 'b', callback_b)
        :tag('opt1'):v_opt('--key')
        ---@diagnostic disable-next-line: param-type-mismatch
        :run() -- find command and lauch the cmd handler(callback_b)

    assert.same('kvalue', res_opt)
    assert.same('arg', res_arg)
  end)

  -- Known issue - trying to combine subcommand dispatcher and argument receiving
  -- the problem is that pulling arguments(:pop()) before the run() method also
  -- pulls "commands"
  it("vars 2 fetch args to var in pipeline is broken", function()
    local res_opt, res_x, res_y = nil, nil, nil
    local callback = function(_, _) end
    local cmd_b_called = false
    local callback_b = function(w)
      cmd_b_called = true
      res_opt = w:get_var('opt1')
      res_x = w:get_var('x')
      res_y = w:get_var('y')
    end

    local w = new_w({ 'cmd-b', '--key', 'kvalue', 'arg1', 'arg2' })
        :desc('desc A'):cmd('cmd-a', 'a', callback)
        :desc('desc B'):cmd('cmd-b', 'b', callback_b)

    w:tag('opt1'):v_opt('--key') -- value fetched before run cmd callback

    w:tag('x'):pop():v_arg()     -- broke call cmd-b pop_arg make ai++
    w:tag('y'):pop():v_arg()     -- brefore run recognize command

    w:run()

    assert.same(false, cmd_b_called)
    assert.is_nil(res_opt) -- ok
    assert.is_nil(res_x)   -- TODO FIXME
    assert.is_nil(res_y)

    assert.same('cmd-b', w:var('x')) -- from arg0
    assert.same('arg1', w:var('y'))  -- from arg1
    assert.same('kvalue', w:var('opt1'))
  end)


  it("vars 2 fetch args to var inside handler", function()
    local w = new_w({ '--key', 'kvalue', 'arg1', 'arg2' })
        :tag('opt1'):v_opt('--key')
        :tag('x'):pop():v_arg()
        :tag('y'):pop():v_arg()

    assert.same('kvalue', w:get_var('opt1'))
    assert.same('arg1', w:get_var('x'))
    assert.same('arg2', w:get_var('y'))

    -- var, get_var and direct access to the variable
    assert.same('kvalue', w:var('opt1'))
    assert.same('kvalue', w.vars.opt1)
  end)


  it("radio-button one var for arg and opt", function()
    local w = new_w({ '--key', 'kvalue', 'arg1', 'arg2' })
        :group('A', 1):tag('x'):v_opt('--key')
        :group('A', 1):tag('x'):pop():v_arg()
    assert.same('arg1', w.vars.x)

    -- sequence affects the result
    local v = new_w({ '--key', 'kvalue', 'arg1', 'arg2' })
        :group('A', 1):tag('x'):pop():v_arg()
        :group('A', 1):tag('x'):v_opt('--key')
    assert.same('kvalue', v.vars.x)

    local q = new_w({ '--key', 'kvalue' })
        :group('A', 1):tag('x'):pop():v_arg()
        :group('A', 1):tag('x'):v_opt('--key')
    assert.same('kvalue', q.vars.x)

    local r = new_w({ 'arg1' })
        :group('A', 1):tag('x'):pop():v_arg()
        :group('A', 1):tag('x'):v_opt('--key') -- not override existed value
    assert.same('arg1', r.vars.x)

    local j = new_w({ 'arg1' })
        :group('A', 1):tag('x'):pop():v_arg()
        :group('A', 1):tag('Y'):v_opt('--key')
    assert.same('arg1', j.vars.x)
    assert.same(nil, j.vars.Y)

    local z = new_w({})
        :group('A', 1):tag('x'):pop():v_arg()
        :group('A', 1):tag('x'):v_opt('--key')
    assert.is_nil(z.vars.x)
  end)


  --
  it("pre_cmd_callback", function()
    local res_opt, res_arg_in_post, res_arg0_pre = nil, nil, nil
    local line = ''
    local callback = function(w)
      line = line .. '#:handler:#'
      res_opt = w.vars.opt1 -- same w:get_var('opt1')
    end
    -- runs before cmdhandler callback, but after moving to next arg(
    -- it first fetch arg and find handler for this cmd run pre and next run cb
    local pre_cb = function(w0)      -- before move deeper
      line = line .. ">:pre:>"
      w0.vars.opt1 = w0:opt('--key') -- alt for w:vars():opt(...)
      res_arg0_pre = w0:_arg(0)      -- without touching
    end
    --
    local post_cb = function(w0)
      line = line .. "<:post:<"
      res_arg_in_post = w0:arg(0)
    end

    local w = new_w({ 'cmd-b', '--key', 'kvalue', 'the-arg' })
        :desc('desc B')
        :cmd('cmd-b', 'b', callback)
        :pre_cmd(pre_cb)
        :post_cmd(post_cb)
        :run() -- find command and lauch the cmd handler(callback_b)

    assert.same('kvalue', res_opt)
    assert.same('the-arg', res_arg0_pre) -- ai already moved to next
    assert.same('the-arg', res_arg_in_post)
    -- sure handler runs after pre_cb
    assert.same('>:pre:>#:handler:#<:post:<', line) -- the call order
    assert.same(false, w:has_input_errors())
    assert.same(false, w:has_unchecked_opts())
  end)

  it("dev_generate -- override and pass the args-line", function()
    local res, res_args, res_obj
    local callback_a = function() res = 'from callback-A' end
    local callback_b = function() res = 'from callback-B' end
    local method_called = false
    local overrides = {
      -- override -- "implements" the interface method from Cmd4Lua
      -- override a stub functions from Cmd4Lua by own implementations
      generate_handle = function(args, obj)
        method_called = true
        res_args = args
        res_obj = obj
      end,
      -- override default function M.Interfaces.parse_line
      parse_line = function(line)
        return parser.parse_line_g(line)
      end,
    }

    local args_for_gen = "cmd arg --key kval arg2"
    local line = 'cmd-a  -_G [' .. args_for_gen .. ']'
    local w = Cmd4Lua.of(line, overrides):set_print_callback(print0)
        :cmd('cmd-a', 'a', callback_a)
        :cmd('cmd-b', 'b', callback_b)
        :run()

    assert.same(overrides.generate_handle, w.interface.generate_handle)
    assert.same(overrides.parse_line, w.interface.parse_line)

    assert.same(false, w:has_input_errors())
    assert.same(true, method_called)
    assert.same('from callback-A', res)

    local exp = { 'cmd', 'arg', '--key', 'kval', 'arg2' }
    assert.same(exp, overrides.parse_line(args_for_gen))
    local exp2 = { 'cmd-a', '-_G', { 'cmd', 'arg', '--key', 'kval', 'arg2' } }
    assert.same(exp2, overrides.parse_line(line))

    assert.same(exp, res_args) -- sure passed in iverrided method
    assert.same(w, res_obj)
  end)


  it("generate_handle implementation", function()
    local res_cb, gen_res_args, gen_res_obj = {}, nil, nil
    -- prepare
    local overrides = {
      -- override -- "implements" the interface method from Cmd4Lua
      generate_handle = function(args, obj)
        --as planned, there should be your own code for processing the "obj"
        -- local w = Cmd4Lua.of(args) and you can work with obj via w
        gen_res_args = args
        gen_res_obj = obj
      end,
    }

    -- "module" with command implementions
    local module = {}
    local cb1 = mk_cmd_callback(res_cb, 'cmd-a', module)
    local cb2 = mk_cmd_callback(res_cb, 'cmd-b', module)

    assert.same(cb1, module.cmd_cmd_a) -- sure callback added to module
    assert.same(cb2, module.cmd_cmd_b)

    ----------------------------- ---------------------------
    -- run cli-app with build-in key -_G - hook inside the :run() method
    local args_for_gen = 'arg2 --key value arg3' -- not supports sub []
    -- local line = 'cmd-a arg1 -_G [' .. args_for_gen .. ']'
    -- NOTE: [] - is a flat string-array block without nested supporting
    local line = 'cmd-a arg1 -_G [' .. args_for_gen .. ']'
    --
    local w = Cmd4Lua.of(line, overrides)
        :set_print_callback(print0)
        :handlers(module)
        -- register commands
        :cmd('cmd-a') -- it will be called as cb1
        :cmd('cmd-b')
        :run()
    ----------------------------- ---------------------------
    --
    assert.same(true, w:has_input_errors())
    assert.same('cmd-a', res_cb.name) -- sure cb1 was be called
    local exp = "Has unexpected Arguments: 'arg1'\n\n"
    assert.same(exp, w:build_input_error_msg())

    assert.same(w, gen_res_obj)
    -- value of the -_G key: to pass into generate_handle for work with obj
    local exp_all_args = {
      'cmd-a', 'arg1', '-_G', { 'arg2', '--key', 'value', 'arg3' }
    }
    assert.same(exp_all_args, parser.parse_line_g(line))

    local exp_args4gen = { 'arg2', '--key', 'value', 'arg3' }
    assert.same(exp_args4gen, gen_res_args) -- sure passed in overrided method

    -- shure args_for_gen passed into own method generate_handle(implementions)
    ---@cast gen_res_args table
    local ingen_w = Cmd4Lua.of(gen_res_args)
    assert.same('arg2', ingen_w:arg(0))
    assert.same('arg3', ingen_w:arg(1))
    assert.same('value', ingen_w:opt('--key'))
  end)


  it("parse_line [](FlatStringArray)", function()
    local parse_line = function(line)
      return parser.parse_line_g(line)
    end
    local args_for_gen = 'a2 --list [a b c] a3'
    local line_array = 'cmd a1 -_G [' .. args_for_gen .. ']'
    local exp = {
      'cmd', 'a1', '-_G', { 'a2', '--list', '[a', 'b', 'c' }, 'a3]' -- flat.
    }
    assert.same(exp, parse_line(line_array))
  end)


  it("pre_work_subscribers", function()
    -- assert.same("", M.pre_work_subscribers())
    local in_cmd, in_work = false, false
    local m = {
      cmd_do_stuff = function(w)
        in_cmd = true
        w:pre_work(false, 'err-message')
        if w:is_input_valid() then
          in_work = true
        end
      end
    }
    ---@diagnostic disable-next-line: unused-local
    local w = new_w('do-stuff x')
        :handlers(m)
        -- register commands
        :cmd('do-stuff') -- it will be called as cb1
        :run()

    assert.same(true, in_cmd)
    assert.same(false, in_work)
  end)

  it("many args", function()
    local w = Cmd4Lua.of({ 'a1', 'a2', 'a3' })
    w:set_print_callback(function() end) -- off output to stdout

    assert.same(false, w:is_input_valid())
    local a = w:many():pop():arg(0)
    --          ^^^^ flag to allow more than one arg
    while w:has_args() do
      a = a .. '-' .. w:pop():arg(0)
    end
    assert.same('a1-a2-a3', a)
  end)

  it("handler is object", function()
    local in_cmd, in_work, obj0 = false, false, nil
    local Class = {
      cmdDoStuff = function(obj, w) -- method name to call instance:cmdDoStuff(w)
        obj0 = obj
        in_cmd = true
        if w:is_input_valid() then
          in_work = true
        end
      end
    }
    local instance = { name = 'unique' }
    ---@diagnostic disable-next-line: unused-local
    local w = new_w('do-stuff')
        :handlers(Class, instance)
        -- register commands
        :cmd('do-stuff') -- it will be called as cb1
        :run()

    assert.same(true, in_cmd)
    assert.same(true, in_work)
    assert.same(instance, obj0)
  end)
end)
