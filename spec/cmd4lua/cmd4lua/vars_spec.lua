-- 16-11-2023 @author Swarg
require 'busted.runner' ()

local conf = require('cmd4lua.settings')
local D = conf.D

describe('cmd4lua vars', function()
  local res_lines = nil
  setup(function()
    -- expose private function for testing
    _G._TEST = true
    Cmd4Lua = require 'cmd4lua'
    HB = require("cmd4lua.help.builder")
    HC = require("cmd4lua.help.collector")
    -- Disable display hints about built-in help
    conf.show_help_hints = false
    conf.devmode = true
  end)

  teardown(function()
    _G._TEST = nil
  end)

  before_each(function()
    D.on = false
    conf.show_help_hints = false
    res_lines = nil
  end)

  after_each(function()
  end)

  local print0 = function(line)
    res_lines = res_lines or {}
    res_lines[#res_lines + 1] = line
  end

  -- new arg wrapper
  ---@param args string|table
  ---@return Cmd4Lua
  ---@param implementions table|nil
  local function nw(args, implementions)
    -- need to override default parse_line function before call new object
    local w = Cmd4Lua.of(args, implementions):set_print_callback(print0)
    ---@cast w Cmd4Lua
    return w
  end

  local asame = assert.same

  it("v_opt", function()
    asame("v0", nw('--key-opt v0'):v_opt('--key-opt'):get_var('key_opt'))
    asame("v1", nw('--key-opt v1'):tag('x'):v_opt('--key-opt'):get_var('x'))
    ---@diagnostic disable-next-line: param-type-mismatch
    asame("v2", nw('--key-opt v2'):tag(nil):v_opt('--key-opt'):get_var('key_opt'))
    asame(nil, nw('--key-opt'):v_opt('--key-opt'):get_var('key_opt'))
  end)

  it("v_optn", function()
    asame(0, nw('--key-opt 0'):v_optn('--key-opt'):get_var('key_opt'))
    asame(1, nw('--key-opt 1'):tag('x'):v_optn('--key-opt'):get_var('x'))
    asame(2, nw('--key-opt 2'):tag(''):v_optn('--key-opt'):get_var('key_opt'))
    asame(nil, nw('--key-opt'):v_optn('--key-opt'):get_var('key_opt'))
  end)

  it("v_optb", function()
    asame(false, nw('--key-opt 0'):v_optb('--key-opt'):get_var('key_opt'))
    asame(true, nw('--key-opt 1'):tag('x'):v_optb('--key-opt'):get_var('x'))
    asame(nil, nw('--key-opt 1'):tag(''):v_optb('--key-opt'):get_var(''))
    asame(nil, nw('--key-opt'):v_optn('--key-opt'):get_var('key_opt'))
  end)

  -- for default parse_line list is a [] not {}
  it("v_optl", function()
    asame({ '1' }, nw('-k [1]'):v_optl('-k'):get_var('k'))
    asame({ '2' }, nw('-k [2]'):tag('x'):v_optl('-k'):get_var('x'))
    asame(nil, nw('-k [4]'):tag(''):v_optl('-k'):get_var(''))
    asame(nil, nw('-k'):v_optl('-k'):get_var('k'))
  end)

  -- "option with handler"
  it("v_optp", function()
    local c = 0
    local h = function(userdata, value)
      userdata[#userdata + 1] = value
      c = c + 1
      return value
    end
    local ud = {}
    asame('1', nw('-k 1'):v_optp('-k', h, ud):get_var('k'))
    asame('2', nw('-k 2'):tag('x'):v_optp('-k', h, ud):get_var('x'))
    asame(nil, nw('-k 3'):tag(''):v_optp('-k', h, ud):get_var(''))
    asame(nil, nw('-k'):v_optp('-k', h, ud):get_var('k'))
    asame(4, c)
    asame({ '1', '2', '3' }, ud)
  end)

  -- The purpose of the validator is to check the value passed through input
  -- before issuing it via return or to self.vars
  -- The purpose of the handler is to call the code to get the value when the
  -- option is specified
  -- Назначение обработчика - получить вызвать код получения значения когда опция указана
  --
  it("optp validate_value_cb vs on_has_opt_handler", function()
    local order, fetch_cb_cnt, validate_cb_cnt = '', 0, 0
    -- handler or suppluer of the value when opt-key passed
    local up = function(ud, value)
      assert.is_nil(ud)
      assert.is_string(value)
      order = order .. '|handler'
      fetch_cb_cnt = fetch_cb_cnt + 1
      return tonumber(value) or value
    end

    -- passed existed value validator
    local v = function(value)
      order = order .. '|validator(' .. tostring(value) .. ')'
      validate_cb_cnt = validate_cb_cnt + 1
      -- valid, errmsg
      return true, nil
    end
    asame(1, nw('-k 1'):ch(v):v_optp('--key', '-k', up, nil):get_var('key'))
    asame('|handler|validator(1)', order)

    -- for optional keys do not call validator if key not specified
    order = ''
    asame(nil, nw('1'):ch(v):v_optp('--key', '-k', up, nil):get_var('key'))
    asame('', order)

    -- for a required opt-key call the validator even if optkey does not specified
    order = ''
    asame(nil, nw('1'):required(v):v_optp('--key', '-k', up, nil):get_var('key'))
    asame('|validator(nil)', order)
  end)

  -- usage example of optp -- provide value when-opt-passed
  it("optp", function()
    local order = ''

    local worker = {
      ---@diagnostic disable-next-line: unused-local
      work = function(self, arg0)
        order = order .. '|worker'
        return '[worker-res:' .. tostring(arg0) .. ']'
      end
    }

    local provider = {
      ---@diagnostic disable-next-line: unused-local
      provide_value = function(self, arg0)
        order = order .. '|provider'
        return '[provide]:' .. tostring(arg0)
      end
    }

    local handle = function(line)
      local w = nw(line)
          :group('A', 1):tag('classname'):pop():v_arg()
          :group('A', 1):desc('Take a ClassName from a current line')
          :tag('classname')
          :v_optp('--from-use', '-u', provider.provide_value, provider)

      if w:is_input_valid() then --  ensure_all_parsed
        return worker:work(w.vars.classname)
      end
    end

    assert.same('[worker-res:x]', handle('x'))
    assert.same('|worker', order)

    order = ''
    assert.same('[worker-res:[provide]:x]', handle('-u x'))
    assert.same('|provider|worker', order)
  end)

  -- features of rewriting variables with and without a group
  it("variables groups", function()
    local handle = function(line)
      local w = nw(line)
          :tag('classname'):optional():pop():v_arg()
          :tag('classname'):v_optp('-u', function() return '@' end)

      if w:is_input_valid() then --  ensure_all_parsed
        return w.vars.classname
      end
      return w:build_input_error_msg()
    end
    assert.is_nil(handle(''))
    assert.is_nil(handle('x')) -- v_optp override value of the v_arg by nil

    assert.same('@', handle('-u x'))

    -- note: without optional will be input error "Expected arg is missing"
  end)

  -- the order directly affects the result
  it("variables groups 2", function()
    local handle = function(line)
      local w = nw(line)
          :tag('classname'):v_optp('-u', function() return '@' end)
          :tag('classname'):optional():pop():v_arg()

      if w:is_input_valid() then --  ensure_all_parsed
        return w.vars.classname
      end
    end
    assert.is_nil(handle(''))
    assert.same('x', handle('x')) -- v_arg override value of the v_opt

    assert.is_nil(handle('-u x'))
  end)

  it("set_var", function()
    local w = nw('')

    assert.same({ root = { key = 8 } }, w:set_var('root.key', 8).vars)
    assert.same({ root = { key = 8, b = 16 } }, w:set_var('root.b', 16).vars)
    w:clear_vars()
    assert.same({}, w.vars)
  end)

  it("variables in sublevels", function()
    local w = nw('cmd value1 subcmd v2')
    assert.same(true, w:is_cmd('cmd'))
    assert.same('value1', w:pop():v_arg():var('arg1'))
    assert.same('value1', w:var('arg1'))
    assert.same('value1', w:get_var('arg1'))
    assert.same('value1', w.vars.arg1)

    -- move deeper to next command context a next sublevel
    assert.same(true, w:is_cmd('subcmd'))
    -- make sure variables do not cleaned
    assert.same(false, conf.VARS_AUTO_CLEAR_VARS_FOR_SUBS)
    assert.same('value1', w.vars.arg1) -- prev

    -- for current ctx first arg is v2 overriden existed value
    assert.same('v2', w:pop():v_arg():var('arg1')) -- new
  end)

  -- Has issues with opt-keys
  -- TODO fix via cmd prefix : and spliting input to subcommand contexts
  it("variables in sublevels and opt-keys", function()
    local w = nw('cmd value1 -k1 v2 subcmd v3 -k2 v4')
    assert.same(true, w:is_cmd('cmd'))
    assert.same('value1', w:pop():v_arg():var('arg1'))
    assert.same('v2', w:v_opt('-k1'):var('k1'))

    -- TODO FIXME has access to subcommands from next subcmd level
    -- restrict access to the arguments of the next subcommand
    assert.same('v4', w:v_opt('-k2'):var('k2'))

    -- move deeper to next command context a next sublevel
    assert.same(true, w:is_cmd('subcmd'))
    -- make sure variables do not cleaned
    assert.same(false, conf.VARS_AUTO_CLEAR_VARS_FOR_SUBS)
    assert.same('value1', w.vars.arg1) -- prev

    -- for current ctx first arg is v2 overriden existed value
    assert.same('v3', w:pop():v_arg():var('arg1')) --new
  end)
end)
