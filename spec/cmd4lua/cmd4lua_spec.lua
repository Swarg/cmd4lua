-- 19-05-2023 @author Swarg
require 'busted.runner' ()

local conf = require('cmd4lua.settings')
local D = conf.D

describe('cmd4lua', function()
  setup(function()
    -- expose private function for testing
    _G._TEST = true
    Cmd4Lua = require("cmd4lua")
    HB = require("cmd4lua.help.builder")
    HC = require("cmd4lua.help.collector")
    -- Disable display hints about built-in help
    conf.show_help_hints = false
  end)

  teardown(function()
    _G._TEST = nil
  end)

  local output = {} -- StdOut

  before_each(function()
    D.on = false
    conf.show_help_hints = false
    output = {}
  end)

  after_each(function()
  end)

  ---@diagnostic disable-next-line: unused-local, unused-function
  local function D_enable(on)
    D.enable_module(conf, on)
  end

  -- redirect print from Stdout inotu "output" table
  ---@param line string
  local print0 = function(line)
    table.insert(output, line)
  end
  -----------------------------------------------------------------------------

  it("new from a line", function()
    -- D.enable()
    local w = Cmd4Lua.of('cmd arg1 arg2')
    assert.is_not_nil(w)
    assert.same('cmd', w:arg(0))
    assert.same('arg1', w:arg(1))
    assert.same('arg2', w:arg(2))
    assert.same(nil, w:arg(3))
    assert.same(nil, w:arg(10))
    assert.same(nil, w:arg(-1))
  end)

  it("new from a table", function()
    local w = Cmd4Lua.of({ 'cmd', 'arg1', 'arg2' })
    assert.is_not_nil(w)
    assert.same('cmd', w:arg(0))
    assert.same('arg1', w:arg(1))
    assert.same('arg2', w:arg(2))
    assert.same(nil, w:arg(3))
    assert.same(nil, w:arg(10))
    assert.same(nil, w:arg(-1))
  end)

  it("new from table", function()
    local w = Cmd4Lua.of('arg1 --list [ el1 el2 el3] arg2')
    assert.is_not_nil(w)
    assert.same('arg1', w:arg(0))
    assert.same('arg2', w:arg(1))
    assert.is_nil(w:opt('--list', '-l'))
    assert.same({ 'el1', 'el2', 'el3' }, w:type('table'):opt('--list', '-l'))
    assert.same(nil, w:arg(3))
  end)

  it("set_print_callback by function", function()
    local w = Cmd4Lua:new():set_print_callback(print0)
    w:desc('trigger-cmd-expected'):is_cmd('cmd')
    assert.same(true, w:show_help_or_errors())
    local exp = [[
The command is expected

Usage:
 <Command>
Commands:
   cmd    -  trigger-cmd-expected
]]
    assert.same(exp, output[1])
  end)


  it("set_print_callback method of instance", function()
    local instance = {
      lines = {},
      print = function(_self, help)
        _self.lines[#_self.lines + 1] = help
      end,
    }
    local w = Cmd4Lua.of({}):set_print_callback(instance.print, instance)
    w:desc('trigger-cmd-expected'):is_cmd('cmd')
    assert.same(true, w:show_help_or_errors())
    local exp = [[
The command is expected

Usage:
 <Command>
Commands:
   cmd    -  trigger-cmd-expected
]]
    assert.same(exp, instance.lines[1])
  end)

  it("about", function()
    local w = Cmd4Lua.of({})
    w:about('This is a description')
    w:about('About the current command level')
    w:desc('desc 1'):is_cmd('cmd-1') -- subcommands
    w:desc('desc 2'):is_cmd('cmd-2')
    local exp = [[
The command is expected

This is a description
About the current command level
Usage:
 <Command>
Commands:
   cmd-1    -  desc 1
   cmd-2    -  desc 2
]]
    assert.same(exp, w:build_help())
  end)

  it("desc append to prev", function()
    local w = Cmd4Lua:new()
    w:desc('the beginning of the description.')
        :desc('continuation of the secription')
        :is_cmd('cmd-1') -- subcommands
    w:desc('short desc'):is_cmd('cmd-2')

    local exp = [[
The command is expected

Usage:
 <Command>
Commands:
   cmd-1    -  the beginning of the description. continuation of the secription
   cmd-2    -  short desc
]]
    assert.same(exp, w:build_help())
  end)


  it("show_help_or_errors - unkwnown", function()
    local help_res
    local cb = function(help) help_res = help end

    local w = Cmd4Lua.of({ 'unkwnown' }):set_print_callback(cb)
    assert.same(false, w:desc('desc 1'):is_cmd('list', 'ls'))
    assert.same(true, w:show_help_or_errors())
    local exp = [[
Unknown Command: 'unkwnown'

Usage:
 <Command>
Commands:
   ls list    -  desc 1
]]
    assert.same(exp, help_res)
  end)

  it("default_cmd + is_cmd", function()
    local w = Cmd4Lua.of({ '-k', 'kval' })
    w:default_cmd('cmd-a')
    assert.same(true, w:is_cmd('cmd-a', 'a'))
    assert.same('kval', w:opt('--key', '-k'))
    assert.same(false, w:has_input_errors())
  end)


  it("default_cmd 2", function()
    local w = Cmd4Lua.of({})
    w:default_cmd('cmd-c')
    assert.same(false, w:is_cmd('cmd-a'))
    assert.same(false, w:is_cmd('cmd-b'))
    assert.same(true, w:is_cmd('cmd-c')) -- then ...
    assert.same(false, w:has_input_errors())
  end)



  it("def + is_cmd fail - has extra args", function()
    local w = Cmd4Lua.of({ 'arg' })
    w:default_cmd('cmd-a')
    assert.same(false, w:desc('d1'):is_cmd('cmd-a', 'a'))
    assert.same(true, w:has_input_errors())
    local exp = "Unknown Command: 'arg'\n\n"
    assert.same(exp, w:build_input_error_msg())
  end)

  it("def + is_cmd fail - extra args - consider as cmd", function()
    local w = Cmd4Lua.of({ 'arg' })
    w:default_cmd('cmd-a')
    assert.same(false, w:is_cmd('cmd-a', 'a'))
    assert.same(true, w:is_cmd('arg'))
    assert.same(false, w:has_input_errors())
  end)

  it("argn", function() --       0    1    2     3     4      5
    local w = Cmd4Lua.of({ 'cmd', '1', '-2', '0', 'abXc', '-2a' })
    assert.is_not_nil(w)
    assert.same('cmd', w:arg(0)) -- 0 to refs to a first arg (current by self.ai)
    assert.same(1, w:argn(1))
    assert.same(-2, w:argn(2))
    assert.same(0, w:argn(3))
    assert.same('abXc', w:arg(4))
    assert.same(nil, w:argn(4))
    assert.same('-2a', w:arg(5))
    assert.is_nil(w:def(16):argn(4)) -- existed arg override default even has wrong val
    assert.same(8, w:def(8):argn(6))
    assert.same(8, w:def(8):argn(6))
    assert.is_nil(w:def(0):argn(5)) -- override by specified arg + convertion err
    assert.same(nil, w:arg(-1))
    assert.same(nil, w:arg(10))
  end)

  it("arg", function()
    local w = Cmd4Lua.of({ 'cmd', '1', '-2', '0', 'ab', '-2a', '--key', 'v' })
    assert.is_not_nil(w)
    assert.same("cmd", w:arg(0))
    assert.same("1", w:arg(1))
    assert.same("-2", w:arg(2))
    assert.same("0", w:arg(3))
    assert.same("ab", w:arg(4))
    assert.same("-2a", w:arg(5))
    assert.is_nil(w:arg(6))
    assert.same(true, w:has_opt('--key'))
    assert.same('v', w:opt('--key')) -- not arg but value of the --key
  end)

  it("argb", function()
    local as_eq = assert.same
    as_eq(true, Cmd4Lua.of({ 'true' }):argb(0))
    as_eq(true, Cmd4Lua.of({ 'TRUE' }):argb(0))
    as_eq(true, Cmd4Lua.of({ 't' }):argb(0))
    as_eq(true, Cmd4Lua.of({ 'T' }):argb(0))
    as_eq(true, Cmd4Lua.of({ '1' }):argb(0))
    as_eq(nil, Cmd4Lua.of({ 'x' }):argb(0))
    as_eq(nil, Cmd4Lua.of({ '' }):argb(0))
    as_eq(nil, Cmd4Lua.of({ 'abc' }):argb(0))
    as_eq(nil, Cmd4Lua.of({ 'true  true' }):argb(0))
    as_eq(nil, Cmd4Lua.of({ 'true-' }):argb(0))
    as_eq(false, Cmd4Lua.of({ 'false' }):argb(0))
    as_eq(false, Cmd4Lua.of({ 'FALSE' }):argb(0))
    as_eq(false, Cmd4Lua.of({ 'f' }):argb(0))
    as_eq(false, Cmd4Lua.of({ 'F' }):argb(0))
    as_eq(false, Cmd4Lua.of({ '0' }):argb(0))
  end)

  it("is_cmd without touch", function()
    local w = Cmd4Lua.of({ 'cmd', 'subcmd' })
    assert.same('cmd', w:_arg(0))
    -- smart touched check and move to next untached
    assert.same(false, w:is_cmd('subcmd'))
  end)

  it("is_cmd", function()
    local w = Cmd4Lua.of({ 'cmd', 'subcmd', '1', '2', 'abc', '-2a' })
    assert.same('cmd', w:arg(0))
    -- smart touched check and move to next untached
    assert.same(true, w:is_cmd('subcmd'))
    assert.same('1', w:arg(0))
    assert.same('2', w:arg(1))
    assert.same(true, w:is_cmd('abc')) -- smart
    assert.same('-2a', w:arg())
  end)

  it("is_help", function()
    local w = Cmd4Lua.of({ 'help' })
    assert.same(true, w:is_help())
    assert.same(false, Cmd4Lua.of({ 'h' }):is_help())
    assert.same(true, Cmd4Lua.of({ '?' }):is_help())
    assert.same(false, Cmd4Lua.of({ '-h' }):is_help())
    assert.same(true, Cmd4Lua.of({ '--help' }):is_help())
    assert.same(false, Cmd4Lua:new():is_help())
  end)


  it("has_args", function()
    assert.same(false, Cmd4Lua.of({}):has_args())
    local w = Cmd4Lua.of({ 'cmd', 'arg1' })
    assert.same(true, w:has_args())
    assert.same(true, w:is_cmd('cmd')) -- inc arg_index
    assert.same(true, w:has_args())    --still arg1
    assert.same('arg1', w:arg())
    w:inc()                            -- next arg
    assert.same(false, w:has_args())   --still arg1
  end)

  local ae = assert.same
  local function is_key(arg)
    --if arg and arg:match('%-[%-]?[^%d]+[%-%w%_]+') -- and not arg:match('%-[%-]?[%d]+.*')
    if arg and arg:match('^%-[%-%w%_]+') and not arg:match('^%-[%-]?[%d]+.*')
    then
      return true
    else
      return false
    end
  end

  it("is_key", function()
    ae(true, is_key('-k'))
    ae(true, is_key('-key'))
    ae(true, is_key('-long-name'))
    ae(true, is_key('--long-key-name'))
    ae(true, is_key('-long_name'))
    ae(false, is_key('-0key'))
    ae(true, is_key('-k0'))
    ae(false, is_key('--0key'))
    ae(true, is_key('--key0'))
    ae(false, is_key('arg-name'))
    ae(false, is_key('a-name'))
    ae(false, is_key('0-name'))
    ae(false, is_key('0--name'))
    ae(false, is_key('0 --name'))
  end)


  it("has_opts", function()
    ae(true, Cmd4Lua.of({ 'arg', '-k' }):has_opts())
    ae(false, Cmd4Lua.of({ 'arg', 'arg2' }):has_opts())
    ae(true, Cmd4Lua.of({ '--key', 'arg2' }):has_opts())
    ae(true, Cmd4Lua.of({ '-k' }):has_opts())
    ae(true, Cmd4Lua.of({ '-k0' }):has_opts())
    ae(false, Cmd4Lua.of({ '-0k' }):has_opts())
    ae(true, Cmd4Lua.of({ '--key0', 'arg2' }):has_opts())
    ae(false, Cmd4Lua.of({ '--0key', 'arg2' }):has_opts())
    ae(false, Cmd4Lua.of({ 'arg-name', 'arg2' }):has_opts())
  end)

  it("has_opts", function()
    ae(true, Cmd4Lua.of({ 'arg', '-k' }):has_opt('-k'))
    ae(false, Cmd4Lua.of({ 'arg', 'arg2' }):has_opt('-k'))
    ae(false, Cmd4Lua.of({ 'arg', 'arg2' }):has_opt('arg'))
    ae(false, Cmd4Lua.of({ 'arg', 'arg2' }):has_opt('arg2'))
    ae(true, Cmd4Lua.of({ '--key', 'arg2' }):has_opt('--key'))
    ae(true, Cmd4Lua.of({ '-k' }):has_opt('-k'))
    ae(true, Cmd4Lua.of({ '-k0' }):has_opt('-k0'))
    ae(false, Cmd4Lua.of({ '-0k' }):has_opt('-0k'))
    ae(true, Cmd4Lua.of({ '--key0', 'arg2' }):has_opt('--key0'))
    ae(false, Cmd4Lua.of({ '--0key', 'arg2' }):has_opt('--0key'))
  end)

  it("opt", function()
    local w = Cmd4Lua.of({ '-k', 'kval', 'arg', '-k2' })
    ae("kval", w:opt('--key', '-k'))
    assert.is_nil(w:opt('--key2', '-k2'))
    assert.is_nil(w:opt('--no-exists-key', '-k3'))
  end)

  it("opt with def no optkey", function()
    local w = Cmd4Lua.of({})
    local val = w:def('def-val'):opt('--key', '-k')
    assert.same('def-val', val) -- no key use default value

    local has, val2 = w:__opt('--key', '-k')
    assert.same(false, has)
    assert.is_nil(val2)
  end)

  it("opt with def has optkey no value", function()
    local w = Cmd4Lua.of({ '--key' })
    local val = w:def('def-val'):opt('--key', '-k')
    local has, val2 = w:__opt('--key', '-k')
    assert.same(true, has)
    assert.is_nil(val2)
    assert.is_nil(val) -- has key without value!
  end)

  it("opt with def", function()
    local w = Cmd4Lua.of({ '-k', 'kval', 'arg', '-k2' })
    ae("kval", w:def('def-val'):opt('--key', '-k'))

    -- NOT use default for the key without a value - return nil
    assert.is_nil(w:def('not-used-def-val'):opt('-k2'))
    -- use defual for not existed optkey
    ae('def-val', w:def('def-val'):opt('--no-exists-key', '-k3'))
  end)

  it("optn with def", function()
    local w = Cmd4Lua.of({ '-k', '1', 'arg', '-kb', '-12' })
    ae(1, w:def(0):optn('--key', '-k'))
    ae(-12, w:def(0):optn('-kb'))
    ae(8, w:def(8):optn('--no-exists-key', '-k3'))
  end)

  it("optb with def", function()
    local w = Cmd4Lua.of({ '-k', '1', 'arg', '-kb', '-12' })

    ae(true, w:def(true):optb('--key', '-k'))

    assert.is_nil(w:def(true):optb('-kb')) -- def
    local exp = {
      name = '-kb',
      type = 'boolean',
      has_default = true,
      default_value = true,
      has_invalid_value = true,
      errmsg = 'Cannot convert string to boolean',
      access_n = 2,
    }
    assert.same(exp, w.collector:getElementMeta())

    ae(true, w:def(true):optb('--no-exists-key', '-k3'))

    ae(true, Cmd4Lua.of({ '-k', 'true' }):def(false):optb('-k'))
    ae(false, Cmd4Lua.of({ '-k', 'false' }):def(true):optb('-k'))
    ae(true, Cmd4Lua.of({ '-k', 'TRUE' }):def(false):optb('-k'))
    ae(true, Cmd4Lua.of({ '-k', 'T' }):def(false):optb('-k'))
    ae(true, Cmd4Lua.of({ '-k', 't' }):def(false):optb('-k'))
    ae(false, Cmd4Lua.of({ '-k', '0' }):def(false):optb('-k'))
    -- not boolean to defualt
    assert.is_nil(Cmd4Lua.of({ '-k', 'abc' }):def(false):optb('-k'))
    assert.is_nil(Cmd4Lua.of({ '-k', 'abc' }):def(true):optb('-k'))
    -- empty value of existed key to defualt
    assert.is_nil(Cmd4Lua.of({ '-k', '-b', 'arg' }):def(true):optb('-k'))
    assert.is_nil(Cmd4Lua.of({ '-k', '-b', 'arg' }):def(false):optb('-k'))
  end)

  it("optl", function()
    local f = Cmd4Lua.of
    assert.same({ 'a', 'b', 'c' }, f('-l [a b c]'):optl('-l'))
    -- optl is a same as :type('list'):opt()
    assert.same({ 'a', 'b' }, f('-l [a b]'):type('list'):opt('-l'))

    assert.same({ 'a' }, f('-l a'):type('list'):opt('-l'))

    -- wrapping from string to "list" works only for list-type:
    assert.is_nil(f('-l a'):type('table'):opt('-l')) -- not for table
    assert.same({ 'a' }, f('-l [a]'):type('table'):opt('-l'))

    assert.same({ 'a b' }, f('-l "a b"'):optl('-l'))
    assert.same({ 'false' }, f('-l false'):optl('-l'))
    assert.same({ '0' }, f('-l 0'):optl('-l'))
    assert.same({}, f('-l []'):optl('-l'))
    assert.same({ '8' }, Cmd4Lua.of('-l [8]'):optl('-l'))
    assert.is_nil(f('-l'):optl('-l'))
    assert.is_nil(f('-l ""'):optl('-l'))

    -- now "annotation" type before optl is ignored. TODO apply for list element
    assert.same({ 'a', 'b' }, f('-l [a b]'):type('string'):optl('-l'))
    -- TODO { 1, 2}
    assert.same({ '1', '2' }, f('-l [1 2]'):type('number'):optl('-l'))
  end)

  it("optl", function()
    local f = Cmd4Lua.of
    assert.same({ '', 'b', '', 'd' }, f('-l ["" b "" d]'):optl('-l'))
  end)

  it("opt + defOptVal", function()
    assert.same(8, Cmd4Lua.of('-k'):defOptVal(8):opt('--key', '-k'))
    assert.same(4, Cmd4Lua.of('-k 4'):defOptVal(8):opt('--key', '-k'))
    assert.same(0, Cmd4Lua.of('-k'):defOptVal(0):opt('--key', '-k'))
    assert.same(false, Cmd4Lua.of('-k'):defOptVal(false):opt('--key', '-k'))
    assert.same('a', Cmd4Lua.of('-k a'):defOptVal('d'):opt('--key', '-k'))
    assert.same({}, Cmd4Lua.of('-k'):defOptVal({}):opt('--key', '-k'))
    assert.same({ 'a' }, Cmd4Lua.of('-k [a]'):defOptVal({}):opt('--key', '-k'))
    assert.same({ '1' }, Cmd4Lua.of('-k [1]'):defOptVal({}):opt('--key', '-k'))
    assert.is_nil(Cmd4Lua.of('-k'):defOptVal(nil):opt('--key', '-k'))
  end)

  it("argl", function()
    local f = Cmd4Lua.of
    assert.same({ 'a', 'b', 'c' }, f('[a b c]'):argl())
    assert.same({ 'a', 'b' }, f('[a b]'):type('list'):arg())

    assert.same({ 'a' }, f('a'):argl())
    assert.same({ 'a b' }, f('"a b"'):argl())
  end)

  it("quoted line", function()
    ae("a b c d", Cmd4Lua.of('"a b c d"'):arg(0))
  end)

  it("opt_override", function()
    local t = { KEY_NAME = "Original-Default-Value", }
    local tkey = "KEY_NAME"

    -- if no optkey - remove entry from table:
    Cmd4Lua:new():opt_override("-o", "--opt", t, tkey)
    ae('Original-Default-Value', t[tkey])

    Cmd4Lua:new():def('Default'):opt_override("-o", "--opt", t, tkey)
    ae("Default", t[tkey])

    Cmd4Lua:new():def(nil):type('any'):opt_override("-o", "--opt", t, tkey)
    ae(nil, t[tkey])

    t = { KEY_NAME = "Original-Default-Value", }
    Cmd4Lua.of({ "-o", "@" }):opt_override("-o", "--opt", t, tkey)
    ae("@", t[tkey])
  end)


  it("opt_override", function()
    local t = { KEY_NAME = "Original-Default-Value", }
    local tkey = "KEY_NAME"

    local w = Cmd4Lua.of({ "-o", "New" })
    assert.same(t.KEY_NAME, t[tkey])

    w:desc('a some description'):opt_override("-o", "--opt", t, 'OPT')
    ae("New", t['OPT'])

    w:desc('another desc'):opt_override("-k", "--key", t, tkey)
    ae(t.KEY_NAME, t[tkey])

    local exp = [[
Usage:
 [Options]
Options:          "TAG"      [Default]
    -k, --key  -  "KEY_NAME" [Original-Default-Value]  another desc
    -o, --opt  -  "OPT"       a some description
]]
    assert.same(exp, w:build_help())
  end)

  -- opt_slist deprecated a new implementation is  :type('table'):opt()
  -- see parse_line_spec.lua


  it("has_input_errors", function()
    local w = Cmd4Lua.of({ "-i", "1", '--int', '2' })
    assert.same(true, w:has_input_errors())
  end)

  it("has_input_errors", function()
    local res = nil
    local cb = function(help) res = help end

    local w = Cmd4Lua.of({ "-i", "1", '--int', '2' }):set_print_callback(cb)

    assert.same(false, w:is_input_valid())
    local exp = "Unknown Input: Opts:  -i --int\n\n"
    assert.same(exp, res)
  end)

  it("has_input_errors obj", function()
    local res, res_self = nil, nil
    local obj = {                  -- emulate an instance of some class
      -- dynamic method of this instance
      print = function(_self, msg) -- obj:print(msg)
        res = msg
        res_self = _self
      end
    }

    local w = Cmd4Lua.of({ "-i", "1", '--int', '2' })
        :set_print_callback(obj.print, obj)

    assert.same(false, w:is_input_valid())
    local exp = "Unknown Input: Opts:  -i --int\n\n"
    assert.same(exp, res)
    assert.same(obj, res_self)
  end)


  it("arg get by offset", function()
    local w = Cmd4Lua.of({ 'a', 'b', '-k', 'v', 'd' })
    assert.same("a", w:arg(0))
    assert.same("b", w:arg(1))
    assert.same("d", w:arg(2))
    assert.is_nil(w:arg(3))
  end)

  it("argn get by offset", function()
    local w = Cmd4Lua.of({ '1', '2', '-k', 'v', '3' })
    assert.same(1, w:argn(0))
    assert.same(2, w:argn(1))
    assert.same(3, w:argn(2))
    assert.is_nil(w:arg(3))
  end)

  it("argb get by offset", function()
    local w = Cmd4Lua.of({ '1', 'true', '-k', 'v', 'false' })
    assert.same(true, w:argb(0))
    assert.same(true, w:argb(1))
    assert.same(false, w:argb(2))
    assert.is_nil(w:arg(3))
  end)

  it("build_help -- no expected argument tag", function()
    local w = Cmd4Lua.of({})
    w:root('cmd')
    local name = w:tag('name'):arg()

    local exp = [[
The expected argument 1 "name" is missing.

Usage:
 cmd <name>
Arguments:
 1  name
]]
    assert.same(exp, w:build_help())
    assert.is_nil(name)
  end)


  it("build_help -- no expected argument desc x2", function()
    local w = Cmd4Lua.of({})
    w:root('cmd')
    local name = w:desc('name', 'the first argument'):arg()

    local exp = [[
The expected argument 1 "name" is missing.

Usage:
 cmd <name>
Arguments:
 1  name             -  the first argument
]]
    assert.same(exp, w:build_help())
    assert.is_nil(name)
  end)


  it("on_has opt", function()
    local w = Cmd4Lua.of({ "-f" })
    local on_has = 16
    local v = w:has_opt('--flag', '-f') and on_has or nil
    assert.same(on_has, v)
  end)

  it("on_has-NO opt", function()
    local w = Cmd4Lua.of({ "" })
    local on_has = 16
    local v = w:has_opt('--flag', '-f') and on_has or nil
    assert.is_nil(v) -- pick the last nil -------------^

    local def_on_hasNO = 88
    v = w:has_opt('--flag', '-f') and on_has or def_on_hasNO
    assert.same(def_on_hasNO, v)
  end)

  it("is_cmd", function()
    local w = Cmd4Lua.of({ "a" })
    assert.same('a', w:arg(0))
    assert.same('a', w:arg())  -- no auto ai++
    assert.same('a', w:arg(0)) -- no auto ai++
    assert.is_nil(w:arg(1))
    --
    assert.same(true, w:is_cmd('a')) -- has auto ai++
    assert.is_nil(w:arg(0))          -- no auto ai++
  end)




  it("inc arg", function()
    local w = Cmd4Lua.of({ 'a', 'b', 'c', 'd', 'arg', 'cmd' })
    assert.same('a', w:arg(0))
    assert.same('a', w:arg(0))
    assert.same('b', w:inc():arg(0))
    assert.same('b', w:arg(0))

    assert.has_error(function() w:pop():arg(1) end)
    assert.same('b', w:pop():arg(0))
    assert.same('c', w:pop_arg()) -- same that pop():arg(0)
    assert.same('d', w:pop():arg())
    assert.same('arg', w:arg(0))
    assert.same('cmd', w:arg(1))
  end)

  it("pop arg", function()
    local w = Cmd4Lua.of({ 'a', 'b', 'c', 'd', 'arg', 'cmd' })
    assert.same('a', w:pop():arg(0))
    assert.same('b', w:arg(0))
    assert.same('b', w:arg(0)) -- sure no auto pop arg from cmd line
    assert.same('b', w:pop():arg(0))
    assert.same('c', w:arg(0))

    assert.has_error(function() w:pop():arg(1) end)
    assert.has_error(function() w:pop():arg(2) end)
    assert.same('c', w:pop():arg(0))
  end)

  it("is_help", function()
    assert.is_true(Cmd4Lua.of({ 'help' }):is_help())
    assert.is_true(Cmd4Lua.of({ '--help' }):is_help())
    assert.is_false(Cmd4Lua.of({ 'h' }):is_help())
    assert.is_false(Cmd4Lua.of({ '-h' }):is_help())
    assert.is_false(Cmd4Lua.of({ '-h', 'x' }):is_help())
    assert.is_false(Cmd4Lua.of({}):is_help())
  end)

  it("constructor pass keys and args", function()
    local args = {
      args = { 'a', 'b', 'c' },
      keys = { ['--key'] = 'kval', ['-j'] = 16 },
      vars = { x = 'value1', y = 8 }
    }
    local w = Cmd4Lua:new(args)
    assert.same("a", w:arg(0))
    assert.same("b", w:arg(1))
    assert.same("c", w:arg(2))
    assert.same("kval", w:opt('--key'))
    assert.same(16, w:optn('-j'))
    assert.is_not_nil(w.vars)
    assert.same('value1', w.vars.x)
    assert.same(8, w.vars.y)
    assert.same(true, w:has_opts())
  end)

  it("constructor empty", function()
    local args = {}
    local w = Cmd4Lua.of(args)
    assert.is_nil(w:arg(0))
    assert.same(false, w:has_opts())
  end)

  it("constructor empty args", function()
    local args = { args = {} }
    local w = Cmd4Lua:new(args)
    assert.is_nil(w:arg(0))
    assert.same(false, w:has_opts())
  end)

  it("constructor has args", function()
    local args = { args = { 'a' } }
    local w = Cmd4Lua:new(args)
    assert.same("a", w:arg(0))
    assert.same(false, w:has_opts())
  end)

  it("constructor empty args", function()
    local args = { args = { 'a' } }
    local w = Cmd4Lua:new(args)
    assert.same("a", w:arg(0))
  end)

  it("constructor no args.args", function()
    local args = {
      -- to correct work required the key "args"
      keys = { ['-j'] = 16 },
    }
    local w = Cmd4Lua:new(args)
    assert.same({}, w.args)
    assert.same(args.keys, w.keys)
  end)

  -- it("constructor fail no args.args", function()
  --   local args = {
  --     invalid_name = { ['-j'] = 16 },
  --   }
  --   assert.has_error(function()
  --     Cmd4Lua:new(args)
  --     -- Error: Has unknown key: "invalid_name" Allowed: args keys vars
  --   end)
  -- end)

  it("constructor auto fetch keys from args.args", function()
    local args = {
      args = { ['-j'] = 16, 'str' },
      keys = { ['--key'] = 8 },
    }
    local w = Cmd4Lua:new(args)
    assert.same({ ['-j'] = 16, 'str' }, w.args)
    w:parse()
    assert.same({ 'str' }, w.args)
    assert.same({ ['-j'] = 16, ['--key'] = 8 }, w.keys)
  end)

  it("constructor override keys from args.args", function()
    local args = {
      args = { ['--key'] = 'overridden', 'str' },
      keys = { ['--key'] = 'original' },
    }
    local w = Cmd4Lua:new(args)
    assert.same({ ['--key'] = 'overridden', 'str' }, w.args)
    w:parse()
    assert.same({ 'str' }, w.args)
    assert.same({ ['--key'] = 'overridden' }, w.keys)
  end)



  -- if has opt call given callback(value-provider) and return value from it
  it("optp - value provider", function()
    local w = Cmd4Lua.of({ '--opt' })

    -- the value provider
    local callback = function(obj, value)
      return obj .. obj .. tostring(value)
    end
    local userdata = 'x'

    assert.same('xxnil', w:optp('--opt', '-o', callback, userdata))
    assert.same(false, w:has_input_errors())

    w = Cmd4Lua.of({ '--opt', 'y' })
    assert.same('xxy', w:optp('--opt', '-o', callback, userdata))
  end)


  it("optp with group(1) has opt", function()
    local w, called = Cmd4Lua.of({ '--opt' }), false
    local callback = function(obj, value)
      called = true; return obj .. ':' .. tostring(value)
    end
    local userdata = 'x'

    assert.same(nil, w:group('A', 1):pop():arg())
    assert.same('x:nil', w:group('A', 1):optp('--opt', '-o', callback, userdata))
    assert.same(true, called)
    assert.same(false, w:has_input_errors())
  end)

  -- fix input on the fly aimed to q
  it("fix_input_on_fly", function()
    local silent = function() end

    local handler = function(line, impls)
      local called = ''
      local w = Cmd4Lua.of(line, impls)
      w:cmd('root', function(w1)
        called = called .. '|root'

        w1:cmd('sub', function(w2)
          called = called .. '|sub:prepare-input'
          if w2:is_input_valid() then
            called = called .. '|sub:work'
          end
        end):run()
      end):run()

      return called
    end

    local w = Cmd4Lua.of('root sub')
    assert.same(true, w:is_cmd('root'))
    assert.same(true, w:is_cmd('sub'))
    assert.same('|root|sub:prepare-input|sub:work', handler('root sub'))
    assert.same('|root', handler('root suXb', { print_callback = silent }))

    local input_fixed = false
    local impls = {
      fix_input_callback = function(o)
        assert.same('root suXb', o.original_input)
        input_fixed = true
        return 'sub'
      end
    }
    -- D.enable()
    assert.same('|root|sub:prepare-input|sub:work', handler('root suXb', impls))
    assert.same(true, input_fixed)

    -- assert.same("", M.fix_input_on_fly('root sub'))
  end)

  it("v_opt_verbose", function()
    local w = Cmd4Lua.of('--verbose'):set_print_callback(print0)
    w:v_opt_verbose()
    assert.same(true, w:is_verbose())

    assert.same({ verbose = 1 }, w.vars)
    assert.same(1, w.vars.verbose)
    assert.same('Verbose message a', w:verbose(1, 'Verbose message', 'a'))
    assert.same({ 'Verbose message a' }, output)

    output = {}
    w = Cmd4Lua.of('--verbose 1'):set_print_callback(print0)
    w:v_opt_verbose()
    assert.same(1, w.vars.verbose)
    assert.same('Verbose message b', w:verbose(1, 'Verbose message', 'b'))
    assert.same({ 'Verbose message b' }, output)

    -- not print - another verbose level
    output = {}
    w = Cmd4Lua.of('--verbose 1'):set_print_callback(print0)
    w:v_opt_verbose()
    assert.same(1, w.vars.verbose)
    assert.same(false, w:verbose(2, 'message lvl 2'))
    assert.same({}, output) -- no output
  end)

  --
  it("v_opt_dry_run is_dry_run", function()
    local w = Cmd4Lua.of('--dry-run'):set_print_callback(print0) ---@type Cmd4Lua
    w:v_opt_dry_run() -- --dry-run
    assert.same(true, w:is_dry_run())

    assert.same({ dry_run = true }, w.vars)
    assert.same(true, w:is_dry_run())
  end)

  --
  it("v_opt_verbose shortname", function()
    local w = Cmd4Lua.of('-v')
    w:v_opt_verbose('-v')
    assert.same(true, w:is_verbose())
  end)

  --
  it("v_opt_dry_run shortname", function()
    assert.is_true(Cmd4Lua.of('-d'):v_opt_dry_run('-d'):is_dry_run())
    assert.is_false(Cmd4Lua.of('-d'):v_opt_dry_run():is_dry_run())
    assert.is_true(Cmd4Lua.of('-D'):v_opt_dry_run('-D'):is_dry_run())
  end)

  --
  it("v_opt_verbose shortname", function()
    assert.is_true(Cmd4Lua.of('--quiet'):v_opt_quiet():is_quiet())
    assert.is_false(Cmd4Lua.of('-q'):v_opt_quiet():is_quiet())
    assert.is_true(Cmd4Lua.of('-q'):v_opt_quiet('-q'):is_quiet())
  end)

  -----------------------------------------------------------------------------

  it("error", function()
    local w = Cmd4Lua.of('some cli command'):set_print_callback(print0)
    assert.same(false, w:has_error())
    assert.same(0, w:exitcode())

    w:error('An error occurred while performing the action')

    assert.same(true, w:has_error())
    assert.same(1, w:exitcode())

    local exp = { "An error occurred while performing the action" }
    assert.same(exp, output)
  end)

  -- Goal: show the presence of input errors for tests that
  -- rely only on w:has_error() and do not checks itseld w:is_input_valid()
  it("treat input_errors as error with specific exitcode", function()
    local w = Cmd4Lua.of('--unknown-key'):set_print_callback(print0)
    assert.same(false, w:is_input_valid()) -- update self.errors()
    assert.same(true, w:has_error())
    assert.same(conf.EXITCODE_INPUT_ERRORS, w:exitcode())
    -- trick for has_error because the reason for the error will still be shown
    -- in the output
    assert.same({ '' }, w.errors)

    local exp = { "Unknown Input: Opts:  --unknown-key\n\n" }
    assert.same(exp, output)
  end)

  it("say consider silence(be quiet)", function()
    local w = Cmd4Lua.of('cmd -q'):set_print_callback(print0)
    w:v_opt_quiet('-q')
    assert.is_nil(w:say('this', 'is', 'simple', 'abc'))

    local w2 = Cmd4Lua.of('cmd'):set_print_callback(print0)
    w2:v_opt_quiet('-q')
    assert.same('this is simple abc', w2:say('this', 'is', 'simple', 'abc'))
  end)

  it("fsay consider silence(be quiet)", function()
    local w = Cmd4Lua.of('cmd -q'):set_print_callback(print0)
    w:v_opt_quiet('-q')
    assert.is_nil(w:fsay('simple string %s', 'abc'))

    local w2 = Cmd4Lua.of('cmd'):set_print_callback(print0)
    w2:v_opt_quiet('-q')
    assert.same('simple string abc', w2:fsay('simple string %s', 'abc'))
  end)

  it("rest_args", function()
    local w = Cmd4Lua.of('cmd arg1 arg2 arg3 -q val a4')
        :set_print_callback(print0)

    -- D_enable(true)
    assert.same(true, w:is_cmd('cmd'))
    assert.same({ 'arg1', 'arg2', 'arg3', 'a4' }, w:rest_args())
  end)

  it("rest_args", function()
    local w = Cmd4Lua.of('cmd arg1 arg2 arg3 -q val a4')
        :set_print_callback(print0)

    -- D_enable(true)
    assert.same(true, w:is_cmd('cmd'))
    assert.same('arg1', w:pop():arg())
    assert.same({ 'arg2', 'arg3', 'a4' }, w:rest_args())
  end)

  it("rest_args no args give empty list", function()
    local w = Cmd4Lua.of('cmd'):set_print_callback(print0)
    assert.same(true, w:is_cmd('cmd'))
    assert.same({}, w:rest_args())
  end)

  it("rest_args no args give empty list with opt key", function()
    local w = Cmd4Lua.of('cmd --key value'):set_print_callback(print0)
    assert.same(true, w:is_cmd('cmd'))
    assert.same({}, w:rest_args())
  end)
end)
