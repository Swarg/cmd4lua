--
require 'busted.runner' ()
local assert = require('luassert')

local base = require('cmd4lua.base')
local conf = require('cmd4lua.settings')
local D = conf.D

describe("cmd4lua.help.builder", function()
  setup(function()
    -- expose private function for testing
    _G._TEST = true
    Cmd4Lua = require("cmd4lua")
    HB = require("cmd4lua.help.builder")
    -- Disable display hints about built-in help
    conf.show_help_hints = false
  end)

  teardown(function()
    _G._TEST = nil
  end)

  after_each(function()
  end)

  before_each(function()
    D.on = false
    conf.WIDTH = 80
    conf.show_help_hints = false
  end)

  it("_build_unknown_opts", function()
    local w = Cmd4Lua.of({ 'cmd', '--help' })
    assert.same(true, w:is_cmd('cmd'))
    assert.is_false(w:has_input_errors())
    assert.same('', HB._build_unknown_opts(w)) --
  end)

  it("_build_unknown_opts", function()
    local w = Cmd4Lua.of({ 'cmd', '--help', '-h' })
    assert.same(true, w:is_cmd('cmd'))
    assert.is_true(w:has_input_errors())
    assert.same("Unknown Input: Opts:  -h\n", HB._build_unknown_opts(w)) --
  end)

  it("_build_unknown_opts", function()
    local w = Cmd4Lua.of({ 'cmd', '--unknown', '-u' })
    assert.same(true, w:is_cmd('cmd'))
    assert.is_true(w:has_input_errors())
    assert.same("Unknown Input: Opts:  -u --unknown\n", HB._build_unknown_opts(w))
  end)

  it("max name and shortname", function()
    local w = Cmd4Lua.of({})
    w:is_cmd("full-name-a", 'sn')
    w:is_cmd("fname-b")
    w:is_cmd("the-full-name-c", 'sn23')
    w:is_cmd("the-full-name-d", 'n2', 'altname')
    local exp = {
      name = 'the-full-name-d',
      access_n = 4,
      shortname = 'n2',
      type = 'string',
      name3 = 'altname',
    }

    assert.same(exp, w.collector:getElementMeta())
    local mn, msn, md = base.get_max_str_leng(
      w.collector.thelp.cmds, 'name', 'shortname', 'description'
    )
    assert.same(15, mn)
    assert.same(4, msn)
    assert.same(0, md)
    local exp2 = [[
Commands:
        fname-b
     sn full-name-a
   sn23 the-full-name-c
     n2 the-full-name-d  altname
]]
    local res = HB._build_help_commands(w)
    assert.same(exp2, res)
  end)

  it("_split_desc", function()
    conf.WIDTH = 40
    local desc = 'assert same true, M.is_instanceof. cmd4lua Cmd4Lua new arg'
    local desc0, lines = HB._split_desc(desc, 10, 10)
    assert.same('assert same true,', desc0)
    local exp = {
      '               M.is_instanceof. cmd4lua',
      '               Cmd4Lua new arg'
    }
    assert.same(exp, lines)
  end)

  it("_split_desc", function()
    conf.WIDTH = 30
    local desc = 'assert same true, M.is_instanceof. cmd4lua Cmd4Lua new arg'
    local desc0, lines = HB._split_desc(desc, 10, 10)
    assert.same('assert', desc0)
    local exp = { '               same true,' }
    assert.same(exp, lines)
  end)

  it("_build_elements_desc", function()
    --local function build_elements_desc(tbl, lines, lmax, props, pmax)
    conf.WIDTH = 40
    local desc = 'assert same true, M.is_instanceof. cmd4lua Cmd4Lua new arg'
    local opts = { { description = desc } }
    local lines, lmax, props, pmax = { '-k --key' }, 8, { '' }, 0
    local exp = [[
-k --key  -  assert same true,
             M.is_instanceof. cmd4lua
             Cmd4Lua new arg
]]
    assert.same(exp, HB._build_elements_desc(opts, lines, lmax, props, pmax, 0))
  end)

  it("fold big desc", function()
    conf.WIDTH = 40
    local desc = 'assert same true, M.is_instanceof. cmd4lua Cmd4Lua new arg'
    local w = Cmd4Lua.of({})
    w:desc(desc):opt('--key', '-k')
    local exp = [[
Usage:
 [Options]
Options:
    -k, --key  -  assert same true,
                  M.is_instanceof.
                  cmd4lua Cmd4Lua new arg
]]
    assert.same(exp, w:build_help())
  end)

  it("build_help -- options", function()
    local w = Cmd4Lua.of({ "-i", "1", '--int', '2' })
    w:group('A', 8):has_opt('-i', '--int', 'description')
    local exp0 = {
      description = 'description',
      access_n = 1,
      group = { type = 8, name = 'A', },
      shortname = '-i',
      required = true,
      name = '--int',
    }

    assert.same(exp0, w.collector:getElementMeta())
    w:has_opt('-f', '--flag')
    w:opt('-a', '--abc', 'a some value')
    w:type('any'):opt('-k', '--key')
    w:type('number'):opt('-n', '--num', 'a number value')
    w:type('table'):opt('-t', '--tbl', 'a table value')
    w:def('val'):opt('-s', '--string', 'a some string value')
    w:env('ENVVAR'):opt('-v', '--var', 'value from opt or sys EnvVar')
    w:def(0):optn('-x', '--x-value', 'a x value')

    -- w:opt_slist('-l', '--list', 'string separated by sep')
    w:type('table'):opt('-l', '--list', 'array-list defined in []')

    local exp = [[
Usage:
 [Options-A] [Options]
Options:                           [Default]
    -a, --abc                   -  a some value
    -f, --flag
*A8 -i, --int                   -  description
    -k, --key
    -l, --list    (table)       -  array-list defined in []
    -n, --num     (number)      -  a number value
    -s, --string                -  [val]  a some string value
    -t, --tbl     (table)       -  a table value
    -v, --var     or ${ENVVAR}  -  value from opt or sys EnvVar
    -x, --x-value (number)      -  [0]  a x value
]]
    assert.same(exp, w:build_help())
  end)

  it("build_help -- env & options", function()
    local w = Cmd4Lua.of({})
    w:required():env('ENVAR_1'):opt('-v', '--value1', 'value from opt or EnvVar')
    w:required():env('ENVAR_2'):opt('-V', '--value2', 'value2 from opt or EnvVar')

    local exp = [[
Not specified the required OptKeys:
  ${ENVAR_2} or OptKey: -V, --value2 is missing  *
  ${ENVAR_1} or OptKey: -v, --value1 is missing  *

Usage:
 [Options]
Options:
*   -v, --value1 or ${ENVAR_1}  -  value from opt or EnvVar
*   -V, --value2 or ${ENVAR_2}  -  value2 from opt or EnvVar

* - Required|Optional. To see more about notation use -_n or --_notations
]]
    conf.show_help_hints = true
    assert.same(exp, w:build_help())
  end)



  it("build_help -- arguments with descriptions and tag", function()
    local w = Cmd4Lua.of({ "a-name", "a-word" })
    w:root('cmd')
    local name = w:tag('name'):desc('the first arg'):arg(0)
    local word = w:tag('word'):desc('a some word'):arg(1)

    local exp = [[
Usage:
 cmd <name> <word>
Arguments:
 1  name             -  the first arg
 2  word             -  a some word
]]
    assert.is_false(w:has_unresolved_cmd())
    assert.same(exp, w:build_help())
    assert.same('a-name', name)
    assert.same('a-word', word)
  end)


  it("build_help -- arguments with descriptions", function()
    local w = Cmd4Lua.of({ "subcmd", "subsubcmd", "a-word" })
    w:root('cmd')
    assert.is_true(w:is_cmd('subcmd'))    -- if is_cmd then...
    assert.is_true(w:is_cmd('subsubcmd')) -- if is_cmd then...
    local word = w:desc('word', 'a some word'):arg(0)

    local exp = [[
Usage:
 cmd subcmd subsubcmd <word>
Arguments:
 1  word             -  a some word
]]
    assert.same(false, w:has_input_errors())
    assert.same(exp, w:build_help())
    assert.same('a-word', word)
  end)

  it("build_help -- arguments with descriptions", function()
    local w = Cmd4Lua.of({ "c1", "c2", "a-word", 'b', 'c3', 'next' })
    assert.is_true(w:is_cmd('c1'))
    assert.is_true(w:is_cmd('c2'))
    assert.same('a-word', w:desc('word1', 'a some word1'):arg(0))
    assert.same('b', w:desc('word2', 'a some word2'):arg(1))
    assert.is_true(w:is_cmd('c3')) -- with smart find next not touched arg

    -- without :pop(), but alredy touched therefore it is considered
    -- as already processed
    assert.same('next', w:_arg())

    local exp = [[
Has unexpected Arguments: 'next'

Usage:
 c1 c2 <word1> <word2> c3
]]
    assert.same(true, w:has_input_errors())
    assert.same(exp, w:build_help())
  end)


  it("build_help -- arguments with descriptions", function()
    local w = Cmd4Lua.of({ "a-name", "a-word" })
    w:root('cmd')
    local name = w:desc('name', 'the first arg'):arg(0)
    local word = w:desc('word', 'a some word'):def('key'):arg(1)

    local exp = [[
Usage:
 cmd <name> <word>
Arguments:
 1  name                            -  the first arg
*2  word             [Default:key]  -  a some word
]]
    assert.same(exp, w:build_help())
    assert.same('a-name', name)
    assert.same('a-word', word)
  end)

  it("build_help -- arguments with default", function()
    local w = Cmd4Lua.of({ "value-of-arg1" })
    w:root('cmd')
    local word = w:tag('word'):def('key'):arg()
    local exp = [[
Usage:
 cmd <word>
Arguments:
*1  word             [Default:key]
]]
    assert.same(exp, w:build_help())
    assert.same('value-of-arg1', word)
  end)

  it("build_help_arguments with default", function()
    local w = Cmd4Lua.of({})
    w:tag('word'):def('very-long-default-value'):arg()
    local exp = [[
Arguments:
*1  word           [Default:very-long-default-value]
]]
    assert.same(exp, HB._build_help_arguments(w))
  end)

  it("build_help_arguments with default", function()
    local w = Cmd4Lua.of({})
    local def = 'very-long-default-value so much so that it ' ..
        'shouldn’t fit into one line'
    w:tag('word'):def(def):arg()
    local exp = [[
Arguments:
*1  word           [Default:very-long-default-value so much so that it shouldn’t fit into one line]
]]
    assert.same(exp, HB._build_help_arguments(w))
  end)

  it("build_help_arguments with default", function()
    local w = Cmd4Lua.of({})
    local def = 'very-long-default-value so much so that it ' ..
        'shouldn’t fit into one line'
    w:tag('word'):desc('the description'):def(def):arg()
    local exp = [[
Arguments:
*1  word                             -  the description
  [Default:very-long-default-value so much so that it shouldn’t fit into one line]
]]
    assert.same(exp, HB._build_help_arguments(w))
  end)

  it("build_help -- arguments without descriptions", function()
    local w = Cmd4Lua.of({ "a-name", "a-word" })
    w:root('cmd')
    local name = w:tag('name'):arg(0)
    local word = w:tag('word'):arg(1)

    local exp = [[
Usage:
 cmd <name> <word>
Arguments:
 1  name
 2  word
]]
    assert.same(exp, w:build_help())
    assert.same('a-name', name)
    assert.same('a-word', word)
  end)

  it("build_help -- arguments without name & descriptions", function()
    local w = Cmd4Lua.of({ "123", "abc" })
    w:root('cmd')
    local a1 = w:arg(0)
    local a2 = w:arg(1)

    local exp = [[
Usage:
 cmd <Arg1> <Arg2>
]]
    assert.same(exp, w:build_help())
    assert.same('123', a1)
    assert.same('abc', a2)
  end)

  it("build_help -- args+opts", function()
    local w = Cmd4Lua.of({ "a-name", "--word", "a-word" })
    w:root('cmd')
    local name = w:desc('name', 'the first arg'):arg(0)
    local word = w:opt('--word', '-w', 'a some word')

    local exp = [[
Usage:
 cmd <name> [Options]
Arguments:
 1  name             -  the first arg
Options:
    -w, --word  -  a some word
]]
    assert.same(exp, w:build_help())
    assert.same('a-name', name)
    assert.same('a-word', word)
  end)

  it("build_help -- args+opts", function()
    local w = Cmd4Lua.of({ "a-name", "--word", "a-word" })
    w:root('cmd')
    local name = w:tag('name'):arg()
    local word = w:opt('--word', '-w', 'a some word')

    local exp = [[
Usage:
 cmd <name> [Options]
Arguments:
 1  name
Options:
    -w, --word  -  a some word
]]
    assert.same(exp, w:build_help())
    assert.same('a-name', name)
    assert.same('a-word', word)
  end)

  -- issue --help and help in a nested subcommands tree
  it("help and a nested sub-command tree", function()
    local w, sub_node_called = nil, true

    local function handle(line)
      return Cmd4Lua.of(line)
          :set_print_callback(function() end) -- hide output
          :cmd('tree', function(w0)
            w0:cmd('node', function(w1)
              w1:cmd('sub-node', function(w2)
                w2:opt('--sub-node-opt')
                if w1:is_input_valid() then
                  sub_node_called = true
                end
              end):run()
            end):run()
          end):run()
    end

    w = handle('tree node sub-node')
    assert.same(true, sub_node_called)
    assert.same(false, w:is_help())
    sub_node_called = false

    w = handle('tree node sub-node help')
    assert.same(false, sub_node_called)
    assert.same(true, w:is_help())
    local exp = [[
Usage:
 tree node sub-node [Options]
Options:
    --sub-node-opt
]]
    assert.same(exp, w:build_help())

    -- if help request is a arg "help" not a key opt "--help"
    w = handle('tree node help sub-node')
    assert.same(false, sub_node_called)
    assert.same(true, w:is_help())
    local exp2 = [[
Usage:
 tree node <Command>
Commands:
   sub-node
]]
    assert.same(exp2, w:build_help())
    -- word --help still arg not a opt
    w = handle('tree node --help sub-node')
    assert.same(exp2, w:build_help())

    w = handle('tree node sub-node --help')
    assert.same(false, sub_node_called)
    assert.same(true, w:is_help())
    assert.same(exp, w:build_help())

    -- if --help and there are arguments that are known commands then
    -- first do transitions to subcommands and then call a help
    -- w = handle('--help tree node sub-node')
    w = handle('--help tree node')
    assert.same(false, sub_node_called)
    assert.same(true, w:is_help())
    -- word --help is not passed from args into optkey.
    local has, help_v = w:__opt('--help')
    assert.is_false(has)
    assert.is_nil(help_v)

    -- here not going into tree node - only print help about tree level
    local exp3 = [[
Has unexpected Arguments: 'node'

Usage:
 <Command>
Commands:
   tree
]]
    assert.same(exp3, w:build_help())
  end)
end)
