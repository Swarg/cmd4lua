--
require 'busted.runner' ()
local assert = require('luassert')
---@diagnostic disable-next-line: unused-local
local conf = require('cmd4lua.settings')

describe("cmd4lua.help.collertor", function()
  setup(function()
    -- expose private function for testing
    _G._TEST = true
    Cmd4Lua = require("cmd4lua")
    D = conf.D
    HB = require("cmd4lua.help.builder")
    -- Disable display hints about built-in help
    conf.show_help_hints = false
  end)

  teardown(function()
    _G._TEST = nil
  end)

  after_each(function()
  end)

  before_each(function()
    D.on = false
    conf.show_help_hints = false
  end)

  it("has_unexpected_args", function()
    assert.same(false, Cmd4Lua:new():has_unexpected_args())
    -- untouched argument - considered as a extra -- trigger help
    assert.same(true, Cmd4Lua.of({ 'arg' }):has_unexpected_args())
    local w = Cmd4Lua.of({ 'arg' })
    assert.same('arg', w:arg(0)) -- touched
    assert.same(false, w:has_unexpected_args())
    assert.same('arg', w:pop():arg(0))
    assert.same(false, w:has_unexpected_args())
  end)

  it("has_unexpected_args", function()
    local w = Cmd4Lua.of({ 'root', 'subcmd', 'arg', 'b', 'cmd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('subcmd'))
    assert.same('arg', w:arg(0))

    assert.is_true(w:has_unexpected_args())
    local exp = "Has unexpected Arguments: 'b' 'cmd'\n\n"
    assert.same(exp, w:build_input_error_msg())

    assert.same('cmd', w:arg(2)) -- a gap!
    -- assert.same('cmd', w:arg(1))
    assert.is_true(w.collector:has_unexpected_args())
    local exp2 = "Has unexpected Arguments: 'b'\n\n"
    assert.same(exp2, w:build_input_error_msg())

    assert.same('b', w:arg(1)) -- a gap!
    assert.is_false(w.collector:has_unexpected_args())
  end)

  it("has_unexpected_args help", function()
    assert.is_true(Cmd4Lua.of({ 'arg' }):has_unexpected_args())
    assert.is_false(Cmd4Lua.of({ 'help' }):has_unexpected_args())
    assert.is_false(Cmd4Lua.of({ '--help' }):has_unexpected_args())
    assert.is_false(Cmd4Lua.of({ '?' }):has_unexpected_args())

    local w = Cmd4Lua.of({ 'help' })
    assert.same(true, w:is_help())
    assert.same(false, w:has_input_errors())
    assert.same('', w:build_help())

    w = Cmd4Lua.of({ '--help' })
    assert.is_true(w:is_help())
    assert.is_false(w:has_input_errors())
    assert.is_false(w.collector:has_unchecked_opts())
    assert.same('', w:build_help())
  end)



  it("argn", function() --       0    1    2     3     4      5
    local w = Cmd4Lua.of({ 'cmd', '1', '-2', '0', 'abXc', '-2a' })
    assert.is_not_nil(w)
    assert.same('cmd', w:arg(0))     -- 0 to refs to a first arg (current by self.ai)
    assert.is_nil(w:def(16):argn(4)) -- existed arg override default even has wrong val
    local exp = {
      ai = 4 + 1,                    -- stored with indexes from 1 not from 0
      type = 'number',
      has_default = true,
      default_value = 16,
      required = false, --?
      has_invalid_value = true,
      errmsg = 'Cannot convert string to number',
      access_n = 2,
    }
    assert.same(exp, w.collector:getElementMeta())

    local val = w:def(8):argn(6)
    assert.same(6 + 1, w.collector:getElementMeta().ai)
    assert.same(8, val)
    assert.same(8, w:def(8):argn(6)) --
    assert.is_nil(w:def(0):argn(5))  -- override by specified arg + convertion err
    assert.same('Cannot convert string to number', w.collector:getElementMeta().errmsg)
    assert.same(nil, w:arg(-1))
    assert.same(nil, w:arg(10))
  end)

  it("opt with def no optkey", function()
    local w = Cmd4Lua.of({})
    w:def('def-val')
    assert.same('def-val', w.collector:getAnnotationMeta().default_value)

    local meta = w.collector:pop_next()
    assert.is_nil(w.collector:getAnnotationMeta().default_value)
    local exp = { default_value = 'def-val', has_default = true }
    assert.same(exp, meta)
    --
    local val = w:def('def-val'):opt('--key', '-k')
    assert.same(nil, w.collector:getAnnotationMeta().default_value) -- pop def in opt
    assert.same('def-val', val)                                     -- no key use default value

    local has, val2 = w:__opt('--key', '-k')
    assert.same(false, has)
    assert.is_nil(val2)
  end)

  -- to define default list(table without non-numberic keys(only index))
  it("optl with def no optkey", function()
    local w = Cmd4Lua.of({})
    w:def({ 'word' })
    assert.same({ 'word' }, w.collector:getAnnotationMeta().default_value)

    local meta = w.collector:pop_next()
    assert.is_nil(w.collector:getAnnotationMeta().default_value)
    local exp = { default_value = { 'word' }, has_default = true }
    assert.same(exp, meta)
    --
    local val = w:def({ 'word' }):optl('--key', '-k')
    assert.same(nil, w.collector:getAnnotationMeta().default_value)
    assert.same({ 'word' }, val)
    assert.same('', w:build_input_error_msg())

    assert.match_error(function()
      local _ = w:def(0):optl('--key', '-k')
    end, 'Invalid default type for "%-%-key". Expected list, has%-def: number')

    assert.match_error(function()
      local _ = w:def({ key = 0 }):optl('--key', '-k')
    end, 'Invalid default type for "%-%-key". Expected list, has%-def: table')

    -- empty table an empty table can also be considered a list
    assert.same({}, w:def({}):optl('--key', '-k'))
    assert.same({}, w:def({}):type('list'):optl('--key', '-k'))
  end)

  it("named element on type error", function()
    local w = Cmd4Lua.of({})

    assert.match_error(function()
      local _ = w:def('str'):argn()
    end, 'Invalid default type for arg#1. Expected number, has%-def: string')

    assert.match_error(function()
      local _ = w:def('str'):tag('key'):argn()
    end, 'Invalid default type for tag:key. Expected number, has%-def: string')

    assert.match_error(function()
      local _ = w:def({ key = 0 }):tag('name'):optl('--key', '-k')
    end, 'Invalid default type for "%-%-key" tag:name. Expected list, has%-def: table')
  end)

  it("optl with def no optkey", function()
    local w = Cmd4Lua.of({})
    assert.same({ 'a' }, w:def({ 'a' }):type('table'):optl('-t'))
    assert.same({ 'a' }, w:def({ 'a' }):type('list'):optl('-t'))

    assert.match_error(function()
      w:def({ key = 'value' }):type('list'):optl('-t')
    end, 'Invalid default type for "%-t". Expected list, has%-def: table')
  end)


  local ae = assert.same

  it("unchecked-unkwnown opts", function()
    local w = Cmd4Lua.of({ "-v", "a", '--value' })
    assert.same({ '-v', '--value' }, w.collector:unchecked_opts())
    assert.same("Unknown Input: Opts:  -v --value\n", HB._build_unknown_opts(w))
    ae('a', w:opt("-v"))
    assert.same({ '--value' }, w.collector:unchecked_opts())
    ae(true, w:has_opt("--value"))
    assert.is_nil(w:opt("--value"))
    assert.is_nil(w.collector:unchecked_opts())
  end)

  it("unchecked-unkwnown opts", function()
    local w = Cmd4Lua.of({ "-i", "1", '--int', '2' })
    assert.same({ '-i', '--int' }, w.collector:unchecked_opts())
    ae('2', w:opt("--int", "-i"))
    assert.is_nil(w.collector:unchecked_opts()) -- passed first opt
    ae(true, w:has_opt("-i"))
    assert.is_nil(w.collector:unchecked_opts())

    ae('1', w:opt("-i", "--int")) -- note! if given short and full opt name
  end)



  it("group opt+arg has_missing_required_args", function()
    local w = Cmd4Lua.of({ "--list" })
    local opt = w:group('A', 1):has_opt('--list')
    local arg = w:group('A', 1):pop():arg(0)

    assert.same(true, opt)
    assert.same(nil, arg)
    assert.same('A', w.collector.thelp.opts['--list'].group.name)
    assert.same('A', w.collector.thelp.args[1].group.name)
    assert.same(false, w.collector:has_missing_required_args())
    assert.same(false, w:has_input_errors())
  end)

  it("group arg+opt has_missing_required_opts", function()
    local w = Cmd4Lua.of({ "arg" })
    local opt = w:group('A', 1):has_opt('--list')
    local arg = w:group('A', 1):pop():arg(0)

    assert.same(false, opt)
    assert.same('arg', arg)
    assert.same('A', w.collector.thelp.opts['--list'].group.name)
    assert.same('A', w.collector.thelp.args[1].group.name)
    assert.same(false, w.collector:has_missing_required_opts())
    assert.same(false, w:has_input_errors())
  end)

  it("group arg+opt has_missing_required_opts", function()
    local w = Cmd4Lua.of({ "arg", "--list" })
    local opt = w:group('A', 1):has_opt('--list')
    local arg = w:group('A', 1):pop():arg(0)

    assert.same(true, opt)
    assert.same('arg', arg)
    assert.same('A', w.collector.thelp.opts['--list'].group.name)
    assert.same('A', w.collector.thelp.args[1].group.name)
    assert.same(false, w.collector:has_missing_required_opts())
    assert.same(false, w.collector:has_missing_required_args())
    -- radio-button-group only one(1) from group
    assert.same(true, w.collector:has_invalid_group())
    assert.same(true, w:has_input_errors())
    local exp = [[
Radio-button Group(1) [A] must has exactly one element, has: 2

Usage:
 [Options-A] <Arg1>
Arguments:
 A1 1
Options:
*A1 --list
]]
    assert.same(exp, w:build_help())
  end)

  it("group no arg+opt", function()
    local w = Cmd4Lua.of({})
    local opt = w:group('A', 1):has_opt('--list')
    local arg = w:group('A', 1):pop():arg(0)

    assert.same(false, opt)
    assert.same(nil, arg)
    assert.same('A', w.collector.thelp.opts['--list'].group.name)
    assert.same('A', w.collector.thelp.args[1].group.name)
    assert.same(true, w.collector:has_missing_required_args())
    assert.same(true, w.collector:has_missing_required_opts())
    assert.same(true, w:has_input_errors())
    local exp = [[
Radio-button Group(1) [A] must has exactly one element, has: 0
You should specify one of the following elements:
The expected argument Arg1 is missing.  A1
The expected OptKey: --list is missing  *A1

Usage:
 [Options-A] <Arg1>
Arguments:
 A1 1
Options:
*A1 --list
]]
    assert.same(exp, w:build_help())
  end)


  it("pop desc & tag", function()
    local w = Cmd4Lua.of({})
    w:tag('varname'):desc('the description')
    local d = w.collector:pop_next()
    assert.same("the description", d.description)
    assert.same("varname", d.tag)
    assert.same({}, w.collector:pop_next())
  end)

  it("pop desc", function()
    local w = Cmd4Lua.of({})
    -- you can define tag with desc then you pass second string in desc method:
    w:desc('varname', 'the description')
    local d = w.collector:pop_next()
    assert.same('the description', d.description)
    assert.same("varname", d.tag)
    assert.same({}, w.collector:pop_next())
  end)

  it("pop desc - autoswap", function()
    local w = Cmd4Lua.of({})
    -- you can define tag with desc then you pass second string in desc method:
    w:desc('the description', 'varname')
    local d = w.collector:pop_next()
    assert.same('the description', d.description)
    assert.same("varname", d.tag)
    assert.same({}, w.collector:pop_next())
  end)

  it("build_help -- required optkey", function()
    local w = Cmd4Lua.of({})
    local key = w:tag('vn_key'):required()
        :env('KEY'):opt('--key', '-k', 'the key description')
    local exp = {
      name = '--key',
      shortname = '-k',
      description = 'the key description',
      type = 'string',
      envvar_name = 'KEY',
      tag = 'vn_key',
      missing = true,
      required = true,
      access_n = 1,
    }
    assert.same(exp, w.collector:getElementMeta())

    assert.is_true(w.collector:has_missing_required_opts())
    assert.is_true(w:has_input_errors())
    local exp_help = [[
The expected "vn_key" ${KEY} or OptKey: -k, --key is missing  *

Usage:
 [Options]
Options:                    "TAG"
*   -k, --key or ${KEY}  -  "vn_key"  the key description

* - Required|Optional. To see more about notation use -_n or --_notations
]]
    conf.show_help_hints = true
    assert.same(exp_help, w:build_help())
    conf.show_help_hints = false
    assert.is_nil(key)
  end)



  it("is_cmd", function()
    local w = Cmd4Lua.of({ "a" })
    local desc = "the description of arg"
    assert.same('a', w:desc(desc):arg(0))
    local exp = {
      type = 'string',
      access_n = 1,
      required = true,
      ai = 1,
      description = 'the description of arg',
    }

    assert.same(exp, w.collector:getElementMeta())
    assert.same('a', w:arg())  -- no auto ai++
    assert.same('a', w:arg(0)) -- no auto ai++
    assert.is_nil(w:arg(1))
  end)

  it("is_cmd desc annotation", function()
    local w = Cmd4Lua.of({ "" })
    local desc = "this is description of first command"
    local desc2 = "the second command description"
    w:desc(desc)
    assert.same(desc, w.collector:getAnnotationMeta().description)
    assert.is_nil(w.collector:getElementMeta())

    -- w:desc(desc):is_cmd(...)
    w:is_cmd("full-name", 'sn')

    local exp0 = {
      name = 'full-name',
      shortname = 'sn',
      type = 'string',
      description = 'this is description of first command',
      access_n = 1,
    }

    assert.same(exp0, w.collector:getElementMeta())
    assert.same({}, w.collector:getAnnotationMeta()) -- empty

    w:desc(desc2):is_cmd("second-cmd", '2')

    local exp = [[
Unknown Command: ''

Usage:
 <Command>
Commands:
   sn full-name     -  this is description of first command
    2 second-cmd    -  the second command description
]]
    assert.same(exp, w:build_help())
  end)

  it("is_cmd 2", function()
    local w = Cmd4Lua.of({ "x" })
    local desc = "this is description of first command"
    local desc2 = "the second command description"
    local desc3 = "the 3th description"
    w:desc(desc):is_cmd("cmd-one", 'o', '1')
    w:desc(desc2):is_cmd("the-cmd-two", 't', '2')
    w:desc(desc3):is_cmd("the-cmd-three", 'cmd', '3')
    local exp = [[
Unknown Command: 'x'

Usage:
 <Command>
Commands:
     o cmd-one        1   -  this is description of first command
   cmd the-cmd-three  3   -  the 3th description
     t the-cmd-two    2   -  the second command description
]]
    assert.same(exp, w:build_help())
  end)

  it("is_cmd 3", function()
    local w = Cmd4Lua.of({ "z" })
    local desc = "this is description of first command"
    local desc3 = "the 3th description"
    w:desc(desc):is_cmd("cmd-one", 'o', '1')
    w:is_cmd("the-cmd-two", 't', '2')
    w:desc(desc3):is_cmd("the-cmd-three", 'cmd', '3')
    local exp = [[
Unknown Command: 'z'

Usage:
 <Command>
Commands:
     o cmd-one        1   -  this is description of first command
   cmd the-cmd-three  3   -  the 3th description
     t the-cmd-two    2
]]
    assert.same(exp, w:build_help())
  end)

  it("is_cmd 4 Unknown cmd", function()
    local w = Cmd4Lua.of({ "bad" })
    local desc = "this is description of the subcommand"
    w:desc(desc):is_cmd("good", 'g')
    assert.is_nil(w.collector._cmds_path)
    local exp = [[
Unknown Command: 'bad'

Usage:
 <Command>
Commands:
   g good    -  this is description of the subcommand
]]
    assert.same(exp, w:build_help())
  end)

  --
  --
  --
  it("is_cmd 5 InDeep Unknown cmd", function()
    local w = Cmd4Lua.of({ "variant-b", "bad" })
    -- root level
    assert.is_false(w:is_cmd("variant-a"))
    -- here switched into deep level like
    assert.is_true(w:desc('short-about cmd varb'):is_cmd("variant-b"))
    -- if w:desc('...'):is_cmd('root_cmd') then
    --    if w:is_cmd('sub_cmd') then ...

    -- update path to deeper sure passet into next level and reset current cmds
    assert.same({ 'variant-b' }, w.collector._cmds_path)
    assert.is_nil(w.collector.thelp.cmds) -- clear prev comds from thelp list

    -- next a subcmd level:
    w:about('A full Description about sub command variant-b for help')
    local desc = "this is a short description of the command"
    w:desc(desc):is_cmd("good", 'g')
    local exp = [[
Unknown Command: 'bad'

A full Description about sub command variant-b for help
Usage:
 variant-b <Command>
Commands:
   g good    -  this is a short description of the command
]]
    assert.same(exp, w:build_help())
  end)

  it("is_cmd 6 Unknown cmd", function()
    local res
    local print0 = function(s) res = s end -- callback
    local w = Cmd4Lua:new({}):set_print_callback(print0)

    w:about('Full Desc') -- here we say - wait to input a command
    w:desc('A'):is_cmd("cmd-a", 'a')
    w:desc('B'):is_cmd("cmd-b", 'b')
    w:desc('C'):is_cmd("cmd-c", 'c')


    -- command input expected - no command found - show a help
    assert.same(false, w:is_input_valid())
    local exp = [[
The command is expected

Full Desc
Usage:
 <Command>
Commands:
   a cmd-a    -  A
   b cmd-b    -  B
   c cmd-c    -  C
]]
    assert.same(exp, res)
  end)

  it("c pass validate_value_callback", function()
    local w = Cmd4Lua.of({ '-k', 'x' })
    local callback = function() end
    assert.same('x', w:ch(callback):opt('-k'))
    assert.same(callback, w.collector:getElementMeta().validate_value_callback)
  end)

  it("env + opt illegal value", function()
    local w = Cmd4Lua.of({ "--key", "X" })
    local cb = function(val)
      return val and val == "y"
    end
    assert.same("X", w:ch(cb):env('KEY'):opt('--key', '-k', 'a description'))
    -- assert.same(true, w:has_input_errors())
    local exp = [[
OptKeys with Invalid values:
  ${KEY}|-k|--key contains invalid value: 'X'

Usage:
 [Options]
Options:
    -k, --key or ${KEY}  -  a description
]]
    assert.same(exp, w:build_help())
  end)

  it("opt_env illegal value + msg", function()
    local w = Cmd4Lua.of({ "--key", "X" })
    local cb = function()
      return false, 'Bad Value'
    end
    assert.same("X", w:ch(cb):env('KEY'):opt('--key', '-k', 'a description'))
    assert.same(true, w:has_input_errors())
    local exp = [[
OptKeys with Invalid values:
  ${KEY}|-k|--key Bad Value: 'X'

Usage:
 [Options]
Options:
    -k, --key or ${KEY}  -  a description
]]
    assert.same(exp, w:build_help())
  end)


  it("arg illegal value", function()
    local w = Cmd4Lua.of({ "X" })
    local cb = function(val)
      local ok = val and val == "y"
      return ok
    end

    assert.same("X", w:desc('arg1', 'the import argument'):ch(cb):arg())
    local exp_thelp = {
      args = { {
        ai = 1,
        tag = 'arg1',
        type = 'string',
        has_invalid_value = true,
        required = true,
        description = 'the import argument',
        invalid_value = 'X',
        validate_value_callback = cb,
        access_n = 1,
      } }
    }
    assert.same(true, w.collector:has_invalid_arg_values())
    assert.same(true, w:has_input_errors())
    assert.same(exp_thelp, w.collector.thelp)
    local exp = [[
1 "arg1" contains invalid value: 'X'

Usage:
 <arg1>
Arguments:
 1  arg1             -  the import argument
]]
    assert.same(exp, w:build_help())
  end)

  it("arg invalid value", function()
    local w = Cmd4Lua.of({ "X" })
    local cb = function(val)
      local ok = val and val == "y"
      return ok, 'Error Message'
    end

    assert.same("X", w:tag('arg1'):desc('the import argument'):ch(cb):arg())
    assert.same(true, w:has_input_errors())
    local exp = [[
1 "arg1" Error Message: 'X'

Usage:
 <arg1>
Arguments:
 1  arg1             -  the import argument
]]
    assert.same(exp, w:build_help())
  end)


  it("no arg", function()
    local w = Cmd4Lua.of({})
    assert.is_nil(w:tag('name'):desc('the desc'):arg(0))
    local exp0 = {
      type = 'string',
      access_n = 1,
      ai = 1,
      tag = 'name',
      missing = true,
      required = true,
      description = 'the desc',
    }

    assert.same(exp0, w.collector:getElementMeta())
    assert.same(true, w:has_input_errors())
    local exp = [[
The expected argument 1 "name" is missing.

Usage:
 <name>
Arguments:
 1  name             -  the desc
]]
    assert.same(exp, w:build_help())
  end)

  it("no arg2 ", function()
    local w = Cmd4Lua.of({ 'a' })
    assert.same('a', w:tag('key'):desc('the key desc'):arg(0))
    assert.is_nil(w:tag('name'):desc('the name desc'):arg(1))
    assert.same(true, w:has_input_errors())
    local exp = [[
The expected argument 2 "name" is missing.

Usage:
 <key> <name>
Arguments:
 1  key              -  the key desc
 2  name             -  the name desc
]]
    assert.same(exp, w:build_help())
  end)

  it("no arg 1 in cmd", function()
    local w = Cmd4Lua.of({ 'cmd', 'subcmd', 'a' })
    assert.is_true(w:is_cmd('cmd'))
    assert.is_true(w:is_cmd('subcmd'))
    assert.same('a', w:tag('key'):desc('the key desc'):arg(0))
    assert.is_nil(w:tag('name'):desc('the name desc'):arg(1))
    assert.same(true, w:has_input_errors())
    local exp = [[
The expected argument 2 "name" is missing.

Usage:
 cmd subcmd <key> <name>
Arguments:
 1  key              -  the key desc
 2  name             -  the name desc
]]
    assert.same(exp, w:build_help())
  end)

  it("no args", function()
    local w = Cmd4Lua.of({})
    assert.is_nil(w:tag('name1'):desc('the desc1'):arg(0))
    assert.is_nil(w:tag('name2'):desc('the desc2'):arg(1))
    assert.same(true, w:has_input_errors())
    local exp = [[
Not specified the required Arguments:
  1 "name1" is missing
  2 "name2" is missing

Usage:
 <name1> <name2>
Arguments:
 1  name1             -  the desc1
 2  name2             -  the desc2
]]
    assert.same(exp, w:build_help())
  end)


  it("opt invalid value", function()
    local w = Cmd4Lua.of({ "-k", "X" })
    local cb = function() return false, 'ErrMsg' end

    assert.same("X", w:desc('vn', 'the import argument'):ch(cb):required():opt('-k'))
    assert.same(true, w:has_input_errors())
    local exp = [[
OptKeys with Invalid values:
  -k "vn" ErrMsg: 'X'

Usage:
 [Options]
Options:   "TAG"
*   -k  -  "vn"  the import argument
]]
    assert.same(exp, w:build_help())
  end)


  it("opt missing required", function()
    local w = Cmd4Lua.of({ "-x", "X" })
    local cb = function() return false, 'ErrMsg' end
    assert.is_nil(w:desc('vn', 'a some opt'):ch(cb):required():opt('-k'))
    assert.same(true, w:has_input_errors())
    local exp = [[
The expected "vn" OptKey: -k is missing  *
Unknown Input: Opts:  -x

Usage:
 [Options]
Options:   "TAG"
*   -k  -  "vn"  a some opt
]]
    assert.same(exp, w:build_help())
  end)

  it("opt missing required", function()
    local w = Cmd4Lua.of({ "-x", "X" })
    local cb = function() return false, 'ErrMsg' end
    assert.is_nil(w:desc('a some opt'):ch(cb):required():opt('-k'))
    assert.same(true, w:has_input_errors())
    local exp = [[
The expected OptKey: -k is missing  *
Unknown Input: Opts:  -x

Usage:
 [Options]
Options:
*   -k  -  a some opt
]]
    assert.same(exp, w:build_help())
  end)

  it("opt missing required", function()
    local w = Cmd4Lua.of({ "-x", "X" })
    local cb = function() return false, 'ErrMsg' end
    assert.is_nil(w:desc('a some opt'):ch(cb):required():opt('--key', '-k'))
    assert.same(true, w:has_input_errors())
    local exp = [[
The expected OptKey: -k, --key is missing  *
Unknown Input: Opts:  -x

Usage:
 [Options]
Options:
*   -k, --key  -  a some opt
]]
    assert.same(exp, w:build_help())
  end)


  it("opt missing required", function()
    local w = Cmd4Lua.of({ "-x", "X" })
    local cb = function() return false, 'ErrMsg' end
    assert.is_nil(w:desc('a some opt'):ch(cb):required():opt_env('EKEY', '-k'))
    assert.same(true, w:has_input_errors())
    local exp = [[
The expected ${EKEY} or OptKey: -k is missing  *
Unknown Input: Opts:  -x

Usage:
 [Options]
Options:
*   -k or ${EKEY}  -  a some opt
]]
    assert.same(exp, w:build_help())
  end)


  it("opt missing required", function()
    local w = Cmd4Lua.of({ "-x", "X" })
    local cb = function() return false, 'ErrMsg' end
    assert.is_nil(w:desc('desc 1'):ch(cb):required():env('KEY_A'):opt('--key-a', '-a'))
    assert.is_nil(w:desc('desc 2'):ch(cb):required():env('KEY_B'):opt('--key-b', '-b'))
    assert.same(true, w:has_input_errors())
    local exp = [[
Not specified the required OptKeys:
  ${KEY_B} or OptKey: -b, --key-b is missing  *
  ${KEY_A} or OptKey: -a, --key-a is missing  *
Unknown Input: Opts:  -x

Usage:
 [Options]
Options:
*   -a, --key-a or ${KEY_A}  -  desc 1
*   -b, --key-b or ${KEY_B}  -  desc 2
]]
    assert.same(exp, w:build_help())
  end)


  it("opt missing required_one_of", function()
    local w = Cmd4Lua.of({})
    assert.is_nil(w:opt('--name', '-n'))
    assert.is_nil(w:group('A', 1):required():env('KEY-A'):opt('--key-a', '-a'))
    assert.is_nil(w:group('A', 1):required():env('KEY-B'):opt('--key-b', '-b'))
    assert.is_nil(w:opt('--non-grouped', '-G'))

    assert.same(true, w:has_input_errors())
    local exp = [[
Radio-button Group(1) [A] must has exactly one element, has: 0
You should specify one of the following elements:
Not specified the required OptKeys:
  ${KEY-A} or OptKey: -a, --key-a is missing  *A1
  ${KEY-B} or OptKey: -b, --key-b is missing  *A1

Usage:
 [Options] [Options-A] [Options]
Options:
*A1 -a, --key-a       or ${KEY-A}
*A1 -b, --key-b       or ${KEY-B}
    -n, --name
    -G, --non-grouped

*?n - Groups. To see more about notation use -_n or --_notations
]]
    conf.show_help_hints = true
    assert.same(exp, w:build_help())
    conf.show_help_hints = false
  end)


  it("opt missing required_one_of 2", function()
    local w = Cmd4Lua.of({ '-b', '2' })
    assert.is_nil(w:group('A', 1):required():env('KA'):opt('--key-a', '-a'))
    assert.same('2', w:group('A', 1):required():env('KB'):opt('--key-b', '-b'))
    assert.same(false, w:has_input_errors())
    local exp = [[
Usage:
 [Options-A]
Options:
*A1 -a, --key-a or ${KA}
*A1 -b, --key-b or ${KB}
]]
    assert.same(exp, w:build_help())
  end)

  it("arg has_unexpected_args fail", function()
    local w = Cmd4Lua.of({ "X", '2' })
    assert.same("X", w:arg(0))
    assert.same(true, w.collector:has_unexpected_args())
    assert.same(true, w:has_input_errors())

    assert.same("2", w:arg(1))
    assert.same(false, w.collector:has_unexpected_args())
    assert.same(false, w:has_input_errors())
  end)

  it("arg has_unexpected_args success", function()
    local w = Cmd4Lua.of({ "X" })
    assert.same("X", w:arg(0))
    assert.same(false, w.collector:has_unexpected_args())
    assert.same(false, w:has_input_errors())
  end)


  it("arg valid value", function()
    local w = Cmd4Lua.of({ "X" })
    local cb = function(val)
      return val and val == "X"
    end
    assert.same("X", w:desc('var1', 'the import argument'):ch(cb):arg(0))
    local exp = {
      args = { {
        ai = 1,
        required = true,
        tag = 'var1',
        type = 'string',
        description = 'the import argument',
        validate_value_callback = cb,
        access_n = 1,
      } }
    }
    assert.same({ 'X' }, w.args)
    assert.same(1, #w.args)
    assert.same(exp, w.collector.thelp)
    assert.same(false, w.collector:has_unexpected_args()) -- no extra args(unckecked)
    assert.same(false, w:has_input_errors())
  end)


  it("optn no value", function()
    local w = Cmd4Lua.of({ "-k" })
    assert.is_nil(w:optn('-k'))
    local exp = {
      access_n = 1,
      errmsg = 'Expected a number value',
      name = '-k',
      type = 'number',
      has_invalid_value = true
    }
    assert.same(exp, w.collector:getElementMeta())
  end)

  it("optn no value with def", function()
    local w = Cmd4Lua.of({ "-k" })
    assert.is_nil(w:def(123):optn('-k'))
    local exp = {
      access_n = 1,
      type = 'number',
      name = '-k',
      errmsg = 'Expected a number value',
      has_invalid_value = true,
      default_value = 123,
      has_default = true
    }
    assert.same(exp, w.collector:getElementMeta())
  end)


  it("optn not a number", function()
    local w = Cmd4Lua.of({ "-k", 'x' })
    assert.is_nil(w:optn('-k'))
    local exp = {
      name = '-k',
      type = 'number',
      has_invalid_value = true,
      errmsg = 'Cannot convert string to number',
      access_n = 1,
    }
    assert.same(exp, w.collector:getElementMeta())
  end)

  it("optn success", function()
    local w = Cmd4Lua.of({ "-k", '16' })
    assert.same(16, w:optn('-k'))
    local exp = { type = 'number', name = '-k', access_n = 1, }
    assert.same(exp, w.collector:getElementMeta())
  end)


  it("opt success", function()
    local w = Cmd4Lua.of({ "-k", 'abc' })
    assert.same('abc', w:opt('-k'))
    local exp = { type = 'string', name = '-k', access_n = 1, }
    assert.same(exp, w.collector:getElementMeta())
  end)

  it("opt no value", function()
    local w = Cmd4Lua.of({ "-k" })
    assert.is_nil(w:opt('-k'))
    local exp = {
      access_n = 1,
      errmsg = 'Expected a string value',
      name = '-k',
      type = 'string',
      has_invalid_value = true
    }
    assert.same(exp, w.collector:getElementMeta())
    assert.same(true, w:has_input_errors())
    local exp2 = [[
OptKeys with Invalid values:
  -k Not Defined (nil)

Usage:
 [Options]
Options:
    -k
]]
    assert.same(exp2, w:build_help())
  end)

  it("opt no value with def", function()
    local w = Cmd4Lua.of({ "-k" })
    assert.is_nil(w:def('def-val'):opt('-k'))
    local exp = {
      access_n = 1,
      type = 'string',
      name = '-k',
      errmsg = 'Expected a string value',
      has_invalid_value = true,
      default_value = 'def-val',
      has_default = true
    }
    assert.same(exp, w.collector:getElementMeta())
  end)

  it("arg with def", function()
    local w = Cmd4Lua.of({})
    assert.same('def-1', w:def('def-1'):arg(0))
    -- no arg take default no need show usage
    assert.same('def-2', w:def('def-2'):arg(1))
    local exp = {
      ai = 2,
      access_n = 2,
      type = 'string',
      has_default = true,
      default_value = 'def-2',
      required = false,
    }

    assert.same(exp, w.collector:getElementMeta())
    assert.same(false, w:has_input_errors())
    local exp_help = [[
Usage:
 <Arg1> <Arg2>
Arguments:
*1              [Default:def-1]
*2              [Default:def-2]
]]
    assert.same(exp_help, w:build_help()) -- clear help inside
    assert.is_true(w.help_showed)
    -- assert.same({}, w.collector.thelp) !!!

    -- no arg - when take default,
    -- def before arg removes a "required" mark from a next element (arg)
    assert.same('def-3', w:def('def-3'):arg(2))
    --
    local exp2 = {
      ai = 3,
      access_n = 3,
      type = 'string',
      has_default = true,
      default_value = 'def-3',
      required = false,
    }
    assert.same(exp2, w.collector:getElementMeta())
    assert.is_false(w:has_missing_required_args())
    -- if there is no optional argument, there is no need to show help
    assert.same(false, w:has_input_errors())
    -- sure no errors
    local exp3 = [[
Usage:
 <Arg1> <Arg2> <Arg3>
Arguments:
*1              [Default:def-1]
*2              [Default:def-2]
*3              [Default:def-3]
]]
    assert.same(exp3, w:build_help())
  end)

  it("optional", function()
    local w = Cmd4Lua.of({ '0', '1' })
    local v1 = w:optional():arg(0)
    assert.same(true, w:has_input_errors())
    local exp = "Arguments:\n*1            [Optional]\n"
    assert.same(exp, HB._build_help_arguments(w))
    assert.same("0", v1)
  end)

  it("cmd_path", function()
    local w = Cmd4Lua.of({ 'cmd', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.same("Usage:\n cmd sub-cmd\n", w:build_usage_line())
  end)

  it("cmd_path with arg", function()
    local w = Cmd4Lua.of({ 'cmd', 'arg', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.same('arg', w:tag('varname'):pop():arg())
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.same("Usage:\n cmd <varname> sub-cmd\n", w:build_usage_line())
  end)

  it("cmd_path with args", function()
    local w = Cmd4Lua.of({ 'cmd', 'arg1', 'arg2' })
    assert.is_true(w:is_cmd('cmd'))

    assert.same('arg1', w:pop():arg())
    assert.same('arg2', w:pop():arg())
    assert.same(false, w:has_input_errors())
  end)

  it("cmd_path with args", function()
    local w = Cmd4Lua.of({ 'cmd', 'arg', '2', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.same('arg', w:tag('varname'):pop():arg())
    assert.same('2', w:tag('vn'):pop():arg())
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.same("Usage:\n cmd <varname> <vn> sub-cmd\n", w:build_usage_line())
  end)

  it("cmd_path with args", function()
    local w = Cmd4Lua.of({ 'cmd', 'arg', '2', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.same('arg', w:tag('varname'):pop():arg())
    assert.same('2', w:tag('vn'):pop():arg())
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.is_false(w:is_cmd('sub-sub-cmd'))
    assert.same("Usage:\n cmd <varname> <vn> sub-cmd <Command>\n", w:build_usage_line())
  end)

  it("cmd_path with args", function()
    local w = Cmd4Lua.of({ 'root', 'cmd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('cmd'))
    assert.is_nil(w:tag('vn'):pop():arg())
    assert.is_false(w:is_cmd('sub-cmd'))
    local exp = "Usage:\n root cmd <vn> <Command>\n"
    assert.same(exp, w:build_usage_line())
  end)

  it("cmd_path with args", function()
    local w = Cmd4Lua.of({ 'root', 'cmd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('cmd'))
    --[[
    if w:is_cmd('sub-cmd') then
       ..
    end
    vn = w:pop():arg()
    -- w:is_cmd(..) or w:pop():arg() == 'x'
    ]]
    assert.is_false(w:is_cmd('sub-cmd'))
    assert.is_nil(w:tag('vn'):pop():arg())
    local exp = "Usage:\n root cmd <Command> <vn>\n"

    assert.same(exp, w:build_usage_line())
  end)

  it("cmd_path with args default", function()
    local w = Cmd4Lua.of({ 'cmd', 'arg', '2', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.same('arg', w:tag('varname'):pop():arg())
    assert.same('2', w:tag('vn'):def('4'):pop():arg()) -- not pass to path
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.same("Usage:\n cmd <varname> sub-cmd\n", w:build_usage_line())
  end)

  it("cmd_path with opt", function()
    local w = Cmd4Lua.of({ 'cmd', '-k', 'kval', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.same('kval', w:opt('--k', '-k'))
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.same("Usage:\n cmd sub-cmd\n", w:build_usage_line())
  end)

  it("cmd_path with required opt", function()
    local w = Cmd4Lua.of({ 'cmd', '-k', 'kval', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.same('kval', w:required():opt('--k', '-k')) --!! required
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.same("Usage:\n cmd <-k|--k> sub-cmd\n", w:build_usage_line())
  end)

  it("cmd_path with required opt", function()
    local w = Cmd4Lua.of({ 'cmd', '-k', 'kval', 'sub-cmd' })
    assert.is_true(w:is_cmd('cmd'))
    assert.same('kval', w:required():env('KEY'):opt('--k', '-k')) --!! required
    assert.is_true(w:is_cmd('sub-cmd'))
    assert.same("Usage:\n cmd <${KEY}|-k|--k> sub-cmd\n", w:build_usage_line())
  end)


  it("group", function()
    local w = Cmd4Lua.of({})
    assert.same({ type = 8, name = 'A' }, w:group('A').collector:getAnnotationMeta().group)
    local B = { name = 'B', type = 1 }
    assert.same(B, w:group(B).collector:getAnnotationMeta().group)
  end)

  it("group 0 (blocked) arg", function()
    local w = Cmd4Lua.of({ 'x' })
    assert.same('x', w:group('C', 0):desc('blocked'):arg(0))
    assert.same(true, w.collector:has_invalid_group())
    local exp = "Specified the element from Blocked Group(0) [C]\n\n"
    assert.same(exp, w:build_input_error_msg())
  end)

  it("group 0 (blocked) opt", function()
    local w = Cmd4Lua.of({ '-x' })
    assert.same(true, w:group('C', 0):desc('blocked'):has_opt('-x'))
    assert.same(true, w.collector:has_invalid_group())
    local exp = "Specified the element from Blocked Group(0) [C]\n\n"
    assert.same(exp, w:build_input_error_msg())
  end)

  it("get_access_queue", function()
    local w = Cmd4Lua.of({})
    w:is_cmd('cmd')
    w:arg(0)
    w:opt('--key', '-k')
    local exp = { {
      access_n = 1,
      elm_type = "Command",
      name = "cmd",
      type = "string"
    }, {
      access_n = 2,
      ai = 1,
      elm_type = "Argument",
      type = 'string',
      missing = true,
      required = true
    }, {
      access_n = 3,
      elm_type = "Options",
      name = "--key",
      shortname = "-k",
      type = "string"
    } }
    -- print("[DEBUG] get_access_queue:", inspect(w:get_access_queue()))
    assert.same(exp, w.collector:get_access_queue())
  end)

  it("move deeper into the subcommands path", function()
    local w = Cmd4Lua.of({ 'a', 'b', 'c', 'd', 'arg', 'cmd' })
    assert.is_true(w:is_cmd('a'))
    assert.is_nil(w.collector:get_actual_cmd()) -- no cmd_expected afret move_deeper
    assert.same('b', w:_arg(0))
    assert.is_false(w.collector.cmd_expected)
    assert.is_false(w:is_cmd('this-triggers cmd_expected'))
    assert.is_true(w.collector.cmd_expected)
    assert.same('b', w.collector:get_actual_cmd())
    assert.same(2, w.collector.lvl_deep)
    assert.same(2, w.ai)

    assert.is_true(w:is_cmd('b'))
    assert.is_false(w.collector.cmd_expected) -- cleaned by passed "b"
    w.collector.cmd_expected = true           -- emulate is_cmd
    assert.same('c', w.collector:get_actual_cmd())
    assert.same(3, w.collector.lvl_deep)
    assert.same(3, w.ai)

    assert.is_true(w:is_cmd('c'))
    w.collector.cmd_expected = true
    assert.same('d', w.collector:get_actual_cmd())
    assert.same(4, w.collector.lvl_deep)
    assert.same(4, w.ai)

    assert.is_true(w:is_cmd('d'))
    w.collector.cmd_expected = true

    assert.same('arg', w.collector:get_actual_cmd())
    assert.same(5, w.collector.lvl_deep)
    assert.same(5, w.ai)

    assert.same('arg', w:_arg(0)) -- without touchs the arg
    assert.is_false(w:is_cmd('cmd'))

    -- arg(0) without :popo() - don't change self.ai, but "touchs" the arg
    assert.same('arg', w:arg(0))
    -- but in is_cmd apply smart findout the index of the next untouched arg
    assert.is_true(w:is_cmd('cmd'))
    w:inc()
    assert.is_false(w:is_cmd('cmd'))
    --
  end)


  it("usage", function()
    local w = Cmd4Lua.of({ 'cmd', '--unknown', '-u' })
    w:usage('This is how to use example')
    w:usage('the second line')

    assert.is_false(w:desc('the desc of command'):is_cmd('another'))

    local exp = [[

Usage Examples:
This is how to use example
the second line
]]
    assert.same(exp, w:get_usage_examples())

    local exp_help = [[
Unknown Command: 'cmd'
Unknown Input: Opts:  -u --unknown

Usage:
 <Command>
Commands:
   another    -  the desc of command

Usage Examples:
This is how to use example
the second line
]]
    assert.same(exp_help, w:build_help())
  end)

  -- sure help data cleaned in move deep in commands hierarhy
  it("autoclear level_data on move_deeper", function()
    local w = Cmd4Lua.of({ 'arg', '-o', '-k', 'v', 'cmd' })
    assert.same('arg', w:pop():arg())
    assert.is_nil(w:opt('-o'))
    assert.same('v', w:required():opt('-k'))

    assert.is_not_nil(w.collector:get_thelp_args())
    assert.is_not_nil(w.collector:get_thelp_opts())
    -- this triggers clean inner thelp - data of the current level(context)
    assert.is_true(w:is_cmd('cmd')) -- move depper

    assert.is_nil(w.collector:get_thelp_args())
    assert.is_nil(w.collector:get_thelp_opts())
    -- required args and opt in the command path also apper in the usage path
    assert.same(' <Arg1> <-k> cmd', HB.build_cmdpath(w))
  end)


  it("untouched smart is_cmd", function()
    local w = Cmd4Lua.of({ 'a', 'b', 'c', 'd', 'cmd', 'subcmd' })
    local a, b, c = w:arg(0), w:arg(1), w:arg(2)
    assert.same('a', a)
    assert.same('b', b)
    assert.same('c', c)
    local off = w.collector:find_next_untouched_arg()
    assert.same(3, off)
    assert.same('d', w:_arg(off)) -- without touch
    assert.is_false(w:is_cmd('cmd'))

    assert.same('d', w:arg(3)) -- touch
    assert.is_true(w:is_cmd('cmd'))
    assert.is_true(w:is_cmd('subcmd'))
    local exp = "Usage:\n <Arg1> <Arg2> <Arg3> <Arg4> cmd subcmd\n"
    assert.same(exp, w:build_help())
  end)

  it("untouched pop is_cmd", function()
    local w = Cmd4Lua.of({ 'a', 'b', 'c', 'd', 'cmd', 'subcmd' })
    local a, b, c = w:pop():arg(), w:pop():arg(), w:pop():arg()
    assert.same('a', a)
    assert.same('b', b)
    assert.same('c', c)

    assert.is_false(w.collector:is_touched_arg(0)) -- 0 current
    assert.is_false(w.collector:is_touched_arg(1)) -- 1 - next
    assert.same('d', w:_arg(0))
    assert.is_false(w:is_cmd('cmd'))

    assert.same('d', w:pop():arg())
    assert.is_true(w:is_cmd('cmd'))
    assert.is_true(w:is_cmd('subcmd'))
    local exp = "Usage:\n <Arg1> <Arg2> <Arg3> <Arg4> cmd subcmd\n"
    assert.same(exp, w:build_help())
  end)

  it("untouched pop is_cmd", function()
    local w = Cmd4Lua.of({ 'a', --[[ 'b', ]] 'cmd' })

    assert.same('a', w:optional():pop():arg())
    assert.same('cmd', w:def('b'):pop():arg())
    assert.is_false(w:is_cmd('cmd'))
    -- TODO add support to split cmd lvl to context by ':' prefix for commands
    -- to support such behavior:
    -- local w = Cmd4Lua.of({ 'a', --[[ 'b', ]] ':cmd' })
    --                                              ^
    -- assert.same('a', w:optional():pop():arg())   |
    -- assert.same('b', w:def('b'):pop():arg())     |
    -- assert.is_true(w:is_cmd('cmd'))         command prefix
  end)

  it("irgnore gaps for is_cmd", function()
    --                         0    1   2     3    4       5
    local w = Cmd4Lua.of({ 'a', 'b', 'c', 'd', 'cmd', 'subcmd' })

    assert.is_false(w.collector:is_touched_arg(0))
    assert.is_false(w.collector:is_touched_arg(1))
    assert.is_false(w.collector:is_touched_arg(2))
    assert.is_false(w.collector:is_touched_arg(3))
    --
    assert.same('d', w:arg(3))
    assert.is_true(w.collector:is_touched_arg(3))

    assert.is_true(w:is_cmd('cmd'))
    assert.is_true(w.collector:has_unexpected_args())
    assert.is_true(w:has_input_errors())
    ----
    local exp = "Has unexpected Arguments: 'subcmd'\n\n"
    assert.same(exp, w:build_input_error_msg())
    local exp2 = [[
Has unexpected Arguments: 'subcmd'

Usage:
 <Arg4> cmd
]]
    -- NOTE: in the Usage there is a gap
    -- and you can see the lack of use of arguments up to the fourth
    -- Do I need to show this somehow in the Usage?
    assert.same(exp2, w:build_help())
  end)

  -- Notes:
  -- if you touch an argument that might actually be an expected command then
  -- this blocks it from being recognized as a command
  --
  it("touched and poped args and is_cmd", function()
    local w = Cmd4Lua.of({ 'a', 'b', 'cmd', 'd' })
    assert.same('a', w:arg(0))
    assert.same('b', w:arg(1))
    assert.same('cmd', w:arg(2))
    assert.same('d', w:arg(3))
    assert.is_true(w:is_cmd('cmd')) -- smart: can ckeck all previous touched args
    --
    --if you didn’t just touch but pulled out then there is no turning back
    w = Cmd4Lua.of({ 'a', 'b', 'cmd', 'd' })
    assert.same('a', w:pop():arg())
    assert.same('b', w:pop():arg())
    assert.same('cmd', w:pop():arg())
    assert.same('d', w:pop():arg())
    assert.is_false(w:is_cmd('cmd')) -- can`t turning back to poped args
  end)

  it("touched and poped args and is_cmd", function()
    --                          1       2        3    4    5        6
    local w = Cmd4Lua.of({ 'root', 'subcmd', 'a', 'b', 'cmd', 'd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('subcmd'))
    assert.same(3, w.ai)
    assert.same('a', w:arg(0)) -- offset from current lvl_deep=3
    assert.same('b', w:arg(1))
    assert.same('cmd', w:arg(2))
    assert.same('d', w:arg(3))
    assert.is_true(w:is_cmd('cmd')) -- smart: can ckeck all previous touched args
    --
    --if you didn’t just touch but pulled out then there is no turning back
    w = Cmd4Lua.of({ 'root', 'subcmd', 'a', 'b', 'cmd', 'd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('subcmd'))
    assert.same('a', w:pop():arg())
    assert.same('b', w:pop():arg())
    assert.same('cmd', w:pop():arg())
    assert.same('d', w:pop():arg())
    assert.is_false(w:is_cmd('cmd')) -- can`t turning back to poped args
  end)

  it("get_actual_cmd", function()
    local w = Cmd4Lua.of({ 'root' })
    assert.is_false(w:is_cmd('cmd'))
    assert.same('root', w.collector:get_actual_cmd())
  end)

  it("get_actual_cmd simple with pop(ai++)", function()
    local w = Cmd4Lua.of({ 'root', 'subcmd', 'a', 'b', 'unknown-cmd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('subcmd'))
    assert.same('a', w:pop():arg())
    assert.same('b', w:pop():arg())
    assert.is_false(w:is_cmd('cmd'))
    assert.same('unknown-cmd', w.collector:get_actual_cmd()) -- simple based w.ai
  end)

  it("get_actual_cmd smart(only touch without pop)", function()
    local w = Cmd4Lua.of({ 'root', 'subcmd', 'a', 'b', 'unknown-cmd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('subcmd'))
    assert.same('a', w:arg(0))
    assert.same('b', w:arg(1))
    assert.is_false(w:is_cmd('cmd'))
    -- will see that the next two arguments from the w.ai were accessed
    -- exactly as arguments (through a arg() method) therefore take next arg
    assert.same('unknown-cmd', w.collector:get_actual_cmd()) -- smart! based untouched
  end)

  it("get_actual_cmd smart(only touch without pop)", function()
    local w = Cmd4Lua.of({ 'root', 'subcmd', 'a', 'unknown-cmd' })
    assert.is_true(w:is_cmd('root'))
    assert.is_true(w:is_cmd('subcmd'))
    assert.same('a', w:arg(0))
    assert.same('unknown-cmd', w:arg(1))
    assert.is_false(w:is_cmd('cmd'))
    -- Since there is no free untouched argument, show the current index as is
    assert.same('a', w.collector:get_actual_cmd())                     -- simple
    --??? Or is it better to say that there is no expected command?
    assert.same("Unknown Command: 'a'\n\n", w:build_input_error_msg()) -- simple
  end)

  it("prev_lvl_cmd_desc", function()
    local w = Cmd4Lua.of({ 'root', 'subcmd', 'a', 'unknown-cmd' })
    local desc
    local called = false
    assert.is_nil(w.collector.thelp.parent_desc)

    desc = 'the desc of the root cmd'
    if w:desc(desc):is_cmd('root') then
      assert.same(desc, w.collector.thelp.parent_desc)
      assert.is_nil(w.collector.thelp.cmd_lvl_desc)
      called = true
      assert.same("the desc of the root cmd\n", HB._build_level_description(w))
    end
    assert.same(true, called)
  end)

  it("optp with group(1) has arg", function()
    local w, called = Cmd4Lua.of({ 'arg' }), false
    local callback = function(obj)
      called = true
      return obj .. obj
    end
    local userdata = 'x'

    assert.same('arg', w:group('A', 1):pop():arg())
    assert.same(nil, w:group('A', 1):optp('--opt', '-o', callback, userdata))
    assert.same(false, called)
    assert.same(false, w:has_input_errors())
  end)


  it("optp with group(1) has no arg and no opt", function()
    local w, called = Cmd4Lua.of({}), false
    local callback = function(obj)
      called = true
      return tostring(obj) .. tostring(obj)
    end
    assert.same(nil, w:group('A', 1):pop():arg())
    assert.same(nil, w:group('A', 1):optp('--opt', '-o', callback))
    assert.same(false, called)
    assert.same(true, w:has_input_errors())
    local exp = [[
Radio-button Group(1) [A] must has exactly one element, has: 0
You should specify one of the following elements:
The expected argument Arg1 is missing.  A1
The expected OptKey: -o, --opt is missing  *A1

]]
    assert.same(exp, w:build_input_error_msg())
  end)

  it("optp with group(1) has arg and opt", function()
    local w, called = Cmd4Lua.of({ 'arg', '--opt' }), false
    local callback = function(obj)
      called = true
      return tostring(obj) .. tostring(obj)
    end
    assert.same('arg', w:group('A', 1):pop():arg())
    assert.same('xx', w:group('A', 1):optp('--opt', '-o', callback, 'x'))
    assert.same(true, called)
    assert.same(true, w:has_input_errors())
    local exp = [[
Radio-button Group(1) [A] must has exactly one element, has: 2

]]
    assert.same(exp, w:build_input_error_msg())
  end)

  it("annotation_env", function()
    local prev_getenv = os.getenv
    ---@diagnostic disable-next-line: unused-local, duplicate-set-field
    os.getenv = function(vk) return 'VALUE' end

    local w = Cmd4Lua:new()
    local v = w:env('KEY'):opt('-k')
    os.getenv = prev_getenv -- restore

    local meta = w.collector:getElementMeta()
    assert.same('KEY', meta.envvar_name)
    assert.same("VALUE", v)
  end)

  it("annotation_pre_work_condguard", function()
    local w = Cmd4Lua:new()
    assert.is_nil(w.collector.subscribers)
    w:pre_work(false, 'errmsg')
    assert.is_table(w.collector.subscribers)
    w.collector:clear_level_data()
    assert.is_nil(w.collector.subscribers)
  end)

  it("get_cmd_path", function()
    local w = Cmd4Lua.of('a b c')
    assert.same(true, w:is_cmd('a') and w:is_cmd('b') and w:is_cmd('c'))
    assert.same({ 'a', 'b', 'c' }, w.collector:get_cmd_path())
    assert.same({ 'a', 'b', 'c' }, w:get_cmd_path())
    local path = w:get_cmd_path()
    path[3] = nil
    assert.same({ 'a', 'b' }, path) -- make sure copy
    assert.same({ 'a', 'b', 'c' }, w:get_cmd_path())
  end)

  it("getElementVarName", function()
    local w = Cmd4Lua.of('a --opt-key val --opt2 -k')
    assert.same('a', w:arg(0))
    assert.same("arg1", w.collector:getElementVarName())

    assert.same('val', w:opt('--opt-key'))
    assert.same('opt_key', w.collector:getElementVarName())

    assert.same(nil, w:opt('--opt2'))
    assert.same('opt2', w.collector:getElementVarName())

    assert.same(nil, w:opt('-k'))
    assert.same('k', w.collector:getElementVarName())
  end)

  it("getElementVarName", function()
    local w = Cmd4Lua.of('a --opt-key val --opt2 -k')
    assert.same('a', w:tag('first'):arg(0))
    assert.same("first", w.collector:getElementVarName())

    assert.same('val', w:tag('OPTKEY'):opt('--opt-key'))
    assert.same('OPTKEY', w.collector:getElementVarName())

    assert.same(nil, w:tag('O2'):opt('--opt2'))
    assert.same('O2', w.collector:getElementVarName())

    assert.same(nil, w:tag('K'):opt('-k'))
    assert.same('K', w.collector:getElementVarName())
  end)

  it("annotation_set_valiable", function()
    local w = Cmd4Lua:new()
    w.collector:annotation_set_valiable()
    assert.same(true, w.collector:getAnnotationMeta().variable)

    w.collector:annotation_set_valiable(false)
    assert.same(false, w.collector:getAnnotationMeta().variable)

    w.collector:annotation_set_valiable(nil)
    assert.same(true, w.collector:getAnnotationMeta().variable)
  end)
end)
