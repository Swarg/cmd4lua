--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("cmd4lua.parser")

-- local D = require('cmd4lua.settings').D


describe("cmd4lua.parser", function()
  local ae = assert.same


  it("parse_line_g", function()
    local f = M.parse_line_g
    assert.same({}, f(''))
    assert.same({ 'a', 'b' }, f('a b'))
    assert.same({ 'a', 'b' }, f(' a b '))
    assert.same({ 'a', 'bc', 'd' }, f(' a  bc d '))
    assert.same({ 'bc d' }, f('"bc d"'))
    assert.same({ 'a', 'bc d', 'e' }, f(' a  "bc d" e'))
    assert.same({ 'a', 'bc d', 'e' }, f(" a  'bc d' e"))
    assert.same({ 'a', 'bc" d', 'e' }, f(" a  'bc\" d' e"))

    assert.same({ 'a', "bc 'd e'" }, f(' a  "bc \'d e\'"'))

    assert.same({ 'a', { 'b cd', 'ef' }, 'z' }, f "a [ 'b cd' ef ] z")
    -- not closed - to the end of the line
    assert.same({ 'a', 'bc d e' }, f(' a  "bc d e'))
    -- escaped
    assert.same({ 'a', '\\"bc', 'd', ' e ' }, f(' a  \\"bc d " e "'))

    assert.same({ 'a', { 'b', 'd e' } }, f(' a [ b "d e"]'))
    assert.same({ '[', 'a', ']' }, f(' "[" a "]"'))
    assert.same({ '\\[', 'a', '\\]' }, f(' \\[ a \\]')) -- not support escape

    -- assert.same({ 'a', { 'b', '"d e"' } }, f(' a [ b "d e"'))

    assert.same({ 'a', {}, 'b' }, f(' a []  b '))
    assert.same({ 'a', { 'c' }, 'b' }, f(' a [c]  b '))
    assert.same({ 'a', { 'c', 'd' }, 'b' }, f(' a [c d]  b '))
    assert.same({ 'a', { 'c d' }, 'b' }, f(' a ["c d"]  b '))
    assert.same({ 'a', { 'c d]', 'b' } }, f(' a ["c d]"  b '))
  end)

  it("parse_line_g flat-array", function()
    local f = M.parse_line_g
    assert.same({ '0' }, f('0'))
    assert.same({ {} }, f('[]'))
    assert.same({ {}, {} }, f('[][]'))
    assert.same({ 'a', {}, 'b' }, f('a[]b'))
    assert.same({ {} }, f(' []'))
    assert.same({ {} }, f(' [] '))
    assert.same({ ']', {} }, f(']['))
    assert.same({ ']', { 'a' } }, f(']["a'))
    assert.same({ ']', { 'a', 'c' } }, f(']["a" c'))
    assert.same({ ']', { 'a c' } }, f(']["a c'))
  end)

  -- known issues
  it("parse_line_g not supported variants", function()
    local f = M.parse_line_g
    assert.same({ 'a', { '[b' }, { 'c' }, ']', 'd' }, f(' a [[b] [c]]"  d'))
    -- ' e 'k -> no "e k"
    assert.same({ 'a', '\\"bc', 'd', ' e ', 'k' }, f(' a  \\"bc d " e "k'))

    assert.same({ '(a', 'b)' }, f('(a b)'))
    assert.same({ '{c', 'd', 'e}' }, f('{c d e}'))

    -- logically it should be one line
    assert.same({ 'a', '', 'b' }, f(" a''b "))
    assert.same({ { 'a', '', 'b' } }, f(" [ a''b ] "))

    assert.same({ '0', '', '2', '', '3', '4' }, f('   0"" 2""3  4  '))
    assert.same({ '0', '', '2', ' ', '3', '4' }, f('  0"" 2" "3 4  '))
  end)

  it("parse_line_g with empty lines", function()
    local f = M.parse_line_g
    assert.same({ { '', '2', '', '3' } }, f('["" 2 "" 3]'))
    assert.same({ { '', '2', ' ', '3' } }, f("['' 2 ' ' 3]"))
    assert.same({ { '', '2', ' ', '3', '' } }, f("['' 2 ' ' 3 '']"))

    assert.same({ '', '2', ' ', '3', '' }, f("'' 2 ' ' 3 ''"))
    assert.same({ '', '2', '', '3' }, f('"" 2 "" 3'))
    assert.same({ '0', '', '2', '', '3' }, f('0"" 2"" 3'))

    assert.same({ { 'extra_before', '' } }, f("[extra_before'']"))
    assert.same({ 'extra_before', '' }, f("extra_before''"))
    assert.same({ 'a', {}, 'b' }, f(" a[]b "))
  end)

  it("parse_args_and_keys", function()
    local obj = { args = { 'arg', 'a2', 'a3' } }
    local f = M.parse_args_and_keys
    local t = ''
    ae({ keys = {}, args = { 'arg', 'a2', 'a3' } }, f(obj))

    obj = { args = { '-k1', 'v1', 'a' } }
    ae({ keys = { ['-k1'] = 'v1' }, args = { 'a' } }, f(obj))

    obj = { args = { 'arg-name', 'a' } }
    ae({ keys = {}, args = { 'arg-name', 'a' } }, f(obj))

    obj = { args = { 'a0', '-k1', 'v1', 'a1' } }
    ae({ keys = { ['-k1'] = 'v1' }, args = { 'a0', 'a1' } }, f(obj))

    obj = { args = { '-k0', '-k1', '-k2' } }
    ae({ keys = { ['-k0'] = t, ['-k1'] = t, ['-k2'] = t }, args = {} }, f(obj))

    obj = { args = { '-k0', '-k1', '1', '-k2', '2' } }
    ae({ keys = { ['-k0'] = t, ['-k1'] = '1', ['-k2'] = '2' }, args = {} }, f(obj))

    obj = { args = { '-k0', '-k1', '1', '-k2', '2', '3' } }
    local exp = {
      keys = { ['-k0'] = t, ['-k1'] = '1', ['-k2'] = '2' },
      args = { '3' }
    }
    ae(exp, f(obj))

    obj = { args = { 'arg0', '-k0', '-k1', '1', '-k2', '2', '3' } }
    exp = {
      keys = { ['-k0'] = t, ['-k1'] = '1', ['-k2'] = '2' },
      args = { 'arg0', '3' }
    }
    ae(exp, f(obj))

    obj = { args = { 'arg0', '--key-name', '-k1', '1', '--k', '2', '3' } }
    exp = {
      keys = { ['--key-name'] = t, ['-k1'] = '1', ['--k'] = '2' },
      args = { 'arg0', '3' }
    }
    ae(exp, f(obj))
    obj = { args = { 'cmd', '--key', '-9', 'arg' } }
    exp = {
      keys = { ['--key'] = '-9' },
      args = { 'cmd', 'arg' }
    }
    ae(exp, f(obj))
  end)

  -- word --help keep as argument not a keyopt
  it("parse_args_and_keys --help", function()
    local f = M.parse_args_and_keys
    local obj = { args = { 'cmd', '--help', 'sub-cmd', 'arg' } }
    local exp = {
      keys = {},
      args = { 'cmd', '--help', 'sub-cmd', 'arg' }
    }
    ae(exp, f(obj))
  end)

  it("parse_args_and_keys -- end-of-options", function()
    local f = M.parse_args_and_keys
    local obj = { args = { 'cmd', '--key', 'value', '--', '-arg_with_dash', '-k', 0 } }
    local exp = {
      args = { 'cmd', '-arg_with_dash', '-k', 0 },
      keys = { ['--key'] = 'value' }
    }
    assert.same(exp, f(obj))

    local obj2 = { args = { 'cmd', '--', '--key', 'value' } }
    local exp2 = { args = { 'cmd', '--key', 'value' }, keys = {} }
    assert.same(exp2, f(obj2))
  end)

  it("update_cli_input", function()
    local args = { 'a', 'b' }
    local f = M.update_cli_input
    assert.same(args, (args))
    assert.same('[a b]', f({ '[a', 'b]' }))
    assert.same('"a b"', f({ '"a', 'b"' }))
    assert.same({ 'a b' }, f({ 'a b' }))
    assert.same('"a b" "', f({ 'a b', '"' }))
    assert.same('"a b"', f({ '"a b"' }))
    assert.same("'a b'", f({ "'a b'" }))
    assert.same("'a' b'", f({ "'a' b'" }))   --
    assert.same("'a\" b'", f({ "'a\" b'" })) --
    assert.same("'a\" b'", f({ "a\" b" }))
    assert.same('"a\' b"', f({ "a' b" }))
    assert.same('[a c b]', f({ "[a", "c", "b]" }))
    assert.same('[a "c b" d]', f({ "[a", "c b", "d]" }))
    assert.same("[a 'c b' d]", f({ "[a", "'c b'", "d]" }))
    assert.same('[a "c b" d]', f({ "[a", '"c b"', "d]" }))
  end)

  it("wrap_to_quote_if_need", function()
    local f = M.wrap_to_quote_if_need
    assert.same('"a b"', f('a b'))
    assert.same('"\'a b"', f('\'a b'))
    assert.same('"a b"', f('"a b"'))
    assert.same("'a b'", f("'a b'"))
    assert.same("'ab'", f("'ab'"))
    assert.same(0, f(0))
    assert.is_nil(f(nil))
  end)
end)
