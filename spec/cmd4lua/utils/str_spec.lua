require 'busted.runner' ()
local assert = require('luassert')
local M = require("cmd4lua.utils.str")

describe("cmd4lua.utils.str", function()
  it("back_indexof", function()
    local f = M.back_indexof
    --                123456789
    assert.same(4, f('abc de fg', 1, 6, ' ', 4))
    assert.same(7, f('abc de fg', 1, 9, ' ', 4))
    assert.same(5, f('ab cdefgh', 1, 9, ' ', 4))
    assert.same(1, f('abcdefghi', 1, 9, ' '))
  end)
end)
